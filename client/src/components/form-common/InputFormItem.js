import Form from 'react-bootstrap/Form'
import { StyledErrorMsg } from '../../styles/Style'

// 入力フォームの個々の項目
export default function InputFormItem({
  itemName,
  defaultValue,
  register,
  registerName,
  type,
  placeholder,
  errorMessage,
}) {
  return (
    <Form.Group className="mt-3">
      <Form.Label className="fw-bold">{itemName}</Form.Label>
      <Form.Control
        type={type}
        placeholder={placeholder}
        defaultValue={defaultValue}
        {...register(registerName)}
      />
      <StyledErrorMsg>{errorMessage}</StyledErrorMsg>
    </Form.Group>
  )
}
