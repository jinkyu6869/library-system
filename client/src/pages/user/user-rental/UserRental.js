import React, { useEffect, useState } from 'react'
import {
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Paper,
} from '@mui/material'
import axios from 'axios'
import { RENTAL_BUTTON, RENTAL_TITLE } from '../../../styles/Style'
import ToMyLibraryButton from '../../../components/button/ToMyLibraryButton'
import { formatDate } from '../../../components/form-common/DatePicker'

export default function UserRental() {
  const [data, setData] = useState([])

  const loadData = async () => {
    const response = await axios.get('http://localhost:3001/rental-detail')
    setData(response.data)
  }

  useEffect(() => {
    loadData()
  }, [])

  const handlePeriod = async (row) => {
    const bookID = row.book_ID
    const result = await axios.get(
      `http://localhost:3001/period/checkPeriodFlag/${bookID}`
    )
    switch (result.data.message) {
      case 'ok':
        if (
          window.confirm(
            '貸出資料は、貸出期間内であれば、１回だけ、延長の手続きをした日から15日間の延長ができます。延長しますか？'
          )
        ) {
          axios.put(`http://localhost:3001/period/${bookID}`)
          window.alert('延長完了')
          setTimeout(() => {
            loadData()
          }, 0)
        }
        break
      case 'no':
        window.alert('予約している人がいるため、延長できません。')
        break
      default:
        break
    }
  }

  return (
    <div style={{ border: 'solid 1px #9ED2C6' }}>
      <ToMyLibraryButton />
      <RENTAL_TITLE>貸出状況</RENTAL_TITLE>
      <TableContainer style={{ width: '90vw' }} component={Paper}>
        {data.length === 0 ? (
          <h6 className="text-center">貸出情報がありません。</h6>
        ) : (
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell
                  style={{ color: '#2B4865', fontSize: '15px', width: '5.5em' }}
                >
                  図書ID
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                  タイトル
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                  著者名
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                  出版社名
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                  貸出日
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                  返却日
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow
                  key={row.book_ID}
                  sx={{ '&:lastChild td, &:lastChild th': { border: 0 } }}
                  selected
                >
                  <TableCell>{row.book_ID}</TableCell>
                  <TableCell>{row.title}</TableCell>
                  <TableCell>{row.author}</TableCell>
                  <TableCell>{row.publisher}</TableCell>
                  <TableCell>
                    {formatDate(row.rent_date).replaceAll('-', '/')}
                    {/* {new Date(row.rent_date).toLocaleDateString()} */}
                  </TableCell>
                  <TableCell>
                    {formatDate(row.return_date).replaceAll('-', '/')}
                    {/* {new Date(row.return_date).toLocaleDateString()} */}
                  </TableCell>
                  <TableCell style={{ width: '7em' }}>
                    {row.ex_period_flag === 0 ? (
                      <RENTAL_BUTTON onClick={() => handlePeriod(row)}>
                        延長
                      </RENTAL_BUTTON>
                    ) : (
                      <p
                        style={{
                          textAlign: 'center',
                          paddingTop: '0.7rem',
                        }}
                      >
                        延長済み
                      </p>
                    )}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        )}
      </TableContainer>
    </div>
  )
}
