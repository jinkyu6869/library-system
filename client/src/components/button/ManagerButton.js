import { faBars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { BAR_BUTTON } from '../../styles/Style'
import MenuButton from './MenuButton'
import { MENU_ITEMS } from './MenuItems'
// 管理者専用ボタン
export default function ManagerButton({ path }) {
  return (
    <div>
      <MenuButton items={MENU_ITEMS} path={path}>
        <BAR_BUTTON>
          <FontAwesomeIcon size="sm" className="my-auto mx-2" icon={faBars} />
          管理者メニュー
        </BAR_BUTTON>
      </MenuButton>
    </div>
  )
}
