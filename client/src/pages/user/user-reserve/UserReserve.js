import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material'
import axios from 'axios'
import { useEffect, useState } from 'react'
import ToMyLibraryButton from '../../../components/button/ToMyLibraryButton'
import { formatDate } from '../../../components/form-common/DatePicker'
import { RENTAL_BUTTON, RENTAL_TITLE } from '../../../styles/Style'
import CancelReserve from './CancelReserve'

function UserReserve() {
  const [data, setData] = useState([])
  const [title, setTitle] = useState()
  const [author, setAuthor] = useState()
  const [publisher, setPublisher] = useState()
  const [bookID, setBookID] = useState()

  const loadData = async () => {
    const response = await axios.get(
      'http://localhost:3001/user/reserve-detail'
    )
    setData(response.data)
  }

  useEffect(() => {
    loadData()
  }, [])

  const [isOpen, setIsOpen] = useState(false)

  const handleStatus = (row) => {
    const current = new Date()
    if (row === 0) {
      return '予約順番待ち'
    } else {
      current.setDate(current.getDate() + 7)
      return (
        <div>
          お受取ください。
          <br />
          期限：{current.toLocaleDateString()}
        </div>
      )
    }
  }

  const handleIndexOfReserve = (row) => {
    const bookID = row.book_ID
    axios
      .get(`http://localhost:3001/reserve-index/${bookID}`)
      .then((response) => {
        window.alert('予約順位は： ' + response.data.data)
      })
  }

  const handleCancel = (bookID) => {
    axios.delete(`http://localhost:3001/user/cancel-reserve/${bookID}`)
    window.alert('予約をキャンセルしました。')
    setTimeout(() => {
      setIsOpen(false)
      loadData()
    }, 0)
  }

  return (
    <div>
      <ToMyLibraryButton />
      <div style={{ width: '90vw', border: ' solid 1px #9ED2C6' }}>
        <RENTAL_TITLE>予約状況</RENTAL_TITLE>
        <TableContainer component={Paper}>
          {data.length === 0 ? (
            <h6 className="text-center">予約情報がありません。</h6>
          ) : (
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell
                    style={{
                      color: '#2B4865',
                      fontSize: '15px',
                      width: '5.5em',
                    }}
                  >
                    図書ID
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                    タイトル
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                    著者名
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                    出版社名
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                    予約日
                  </TableCell>
                  <TableCell
                    style={{
                      color: '#2B4865',
                      fontSize: '15px',
                      width: '10em',
                    }}
                  >
                    状況
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {data.map((row, idx) => (
                  <TableRow
                    key={idx}
                    sx={{ '&:lastChild td, &:lastChild th': { border: 0 } }}
                    selected
                  >
                    <TableCell>{row.book_ID}</TableCell>
                    <TableCell>{row.title}</TableCell>
                    <TableCell>{row.author}</TableCell>
                    <TableCell>{row.publisher}</TableCell>
                    <TableCell>
                      {formatDate(row.reserved_date).replaceAll('-', '/')}
                      {/* {new Date(row.reserved_date).toLocaleDateString()} */}
                    </TableCell>
                    <TableCell>{handleStatus(row.status)}</TableCell>
                    <TableCell
                      style={{
                        width: '6em',
                        padding: '0 0 0 10px',
                      }}
                    >
                      <RENTAL_BUTTON onClick={() => handleIndexOfReserve(row)}>
                        予約順番
                      </RENTAL_BUTTON>
                    </TableCell>
                    <TableCell style={{ width: '6em', padding: '0' }}>
                      <RENTAL_BUTTON
                        onClick={() => {
                          setIsOpen(true)
                          setTitle(row.title)
                          setAuthor(row.author)
                          setPublisher(row.publisher)
                          setBookID(row.book_ID)
                        }}
                      >
                        キャンセル
                      </RENTAL_BUTTON>
                    </TableCell>
                    <CancelReserve
                      show={isOpen}
                      close={() => setIsOpen(false)}
                      title={title}
                      author={author}
                      publisher={publisher}
                      cancel={() => handleCancel(bookID)}
                    />
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          )}
        </TableContainer>
      </div>
    </div>
  )
}
// }

export default UserReserve
