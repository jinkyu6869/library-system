import Form from 'react-bootstrap/Form'

// 管理者権限付与チェックボックス
export default function AuthCheckBox({ register }) {
  return (
    <Form.Group className="mt-2">
      <div className="form-check">
        <input
          {...register('authorityCODE')}
          value="true"
          className="form-check-input"
          type="checkbox"
          id="flexCheckDefault"
        />
        <label className="form-check-label" htmlFor="flexCheckDefault">
          登録者に管理者権限を与える。
        </label>
      </div>
    </Form.Group>
  )
}
