import React from 'react'
import { Link } from 'react-router-dom'
import { StyledFormButton } from '../../styles/Style'

export default function ToMyLibraryButton() {
  return (
    <div
      style={{
        position: 'absolute',
        display: 'flex',
        top: '0.5rem',
        left: '2rem',
      }}
    >
      <Link to="/user">
        <StyledFormButton>マイライブラリ</StyledFormButton>
      </Link>
    </div>
  )
}
