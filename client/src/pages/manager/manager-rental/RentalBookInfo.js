import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTriangleExclamation } from '@fortawesome/free-solid-svg-icons'
import { CANCEL_BUTTON, TABLE, TABLE_TD } from '../../../styles/Style'
import axios from 'axios'
import { formatDate } from '../../../components/form-common/DatePicker'

export default function RentalBookInfo({
  setIsOpen,
  data,
  open,
  rentalUserID,
}) {
  const toDay = new Date()
  const plusDays = new Date()
  plusDays.setDate(plusDays.getDate() + 15)
  const register = (row) => {
    const bookID = row.book_ID
    const userID = rentalUserID
    // const userID = inputValue.current.value
    const rentDate = toDay.toLocaleDateString()
    const returnDate = plusDays.toLocaleDateString()

    let r = window.confirm('貸出情報を登録しますか？')
    if (!r) return
    axios
      .put('http://localhost:3001/manager/register-rental', {
        bookID,
        userID,
        rentDate,
        returnDate,
      })
      .then((response) => {
        window.alert('登録完了')
        setIsOpen(false)
      })
  }

  if (!open) return null
  return (
    <>
      {data.length !== 0 ? (
        <div>
          <h4>
            <FontAwesomeIcon className="mt-4" icon={faTriangleExclamation} />{' '}
            貸出情報
          </h4>
          {/* <LINE /> */}
          {data.map((row, idx) => (
            <div style={{ width: '40rem' }} key={idx}>
              <TABLE>
                <tbody>
                  <tr>
                    <TABLE_TD>会員ID</TABLE_TD>
                    <TABLE_TD>{rentalUserID}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>図書ID</TABLE_TD>
                    <TABLE_TD>{row.book_ID}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>タイトル</TABLE_TD>
                    <TABLE_TD>{row.title}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>著者名</TABLE_TD>
                    <TABLE_TD>{row.author}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>出版社</TABLE_TD>
                    <TABLE_TD>{row.publisher}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>貸出日</TABLE_TD>
                    <TABLE_TD>
                      {formatDate(toDay).replaceAll('-', '/')}
                    </TABLE_TD>
                    {/* <TABLE_TD>{toDay.toLocaleString() + ''}</TABLE_TD> */}
                  </tr>
                  <tr>
                    <TABLE_TD>返却日</TABLE_TD>
                    <TABLE_TD>
                      {formatDate(plusDays).replaceAll('-', '/')}
                    </TABLE_TD>
                  </tr>
                </tbody>
              </TABLE>
              <CANCEL_BUTTON className="ms-5" onClick={() => register(row)}>
                登録
              </CANCEL_BUTTON>
            </div>
          ))}
        </div>
      ) : (
        <h3 className="mt-3 text-center">図書情報がありません。</h3>
      )}
    </>
  )
}
