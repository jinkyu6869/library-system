import { useStore } from '../../App'
import { WRAPPER } from '../../styles/Style'

const handleLogout = () => {
  var r = window.confirm('ログアウトしますか？')
  if (r) {
    localStorage.clear()
    useStore.setState({ loginID: '' })
    return '/'
  }
}
export const MENU_ITEMS = [
  {
    title: 'マイライブラリ',
    to: '/user',
  },
  {
    title: '会員情報管理',
    to: '/manager-user',
  },
  {
    title: '図書情報管理',
    to: '/manager-book',
  },
  {
    title: '貸出情報管理',
    to: '/manager-rental',
  },
  {
    title: '予約情報管理',
    to: '/manager-reserve',
  },
  {
    title: 'ログアウト',
    to: '/',
    logout: handleLogout,
  },
]

export function Wrapper({ children, className }) {
  return <WRAPPER className={className}>{children}</WRAPPER>
}
