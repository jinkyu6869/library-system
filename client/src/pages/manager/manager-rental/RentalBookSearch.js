import axios from 'axios'
import React, { useRef, useState } from 'react'
import ReactDOM from 'react-dom'

import {
  CLOSE_BUTTON,
  MODAL_STYLES,
  OVERLAY_STYLES,
} from '../../../styles/Style'
import RentalBookInfo from './RentalBookInfo'

export default function RentalBookSearch({ show, close, rentalUserID }) {
  const inputValue = useRef(null)
  const [data, setData] = useState([])

  const loadData = async () => {
    const bookID = inputValue.current.value
    if (bookID.length !== 5) return
    const response = await axios.get(
      `http://localhost:3001/manager/rental-book-search/${bookID}/${rentalUserID}`
    )
    if (response.data.message === 'rental-ing') {
      alert('貸出中の図書です。')
      return
    } else if (response.data.message === 'reserve-ing') {
      alert('図書を他の人が予約しているか、貸出可能な状態ではありません。')
      return
    } else {
      setData(response.data)
      setIsOpen(true)
    }
  }

  const [isOpen, setIsOpen] = useState(false)

  if (!show) return
  return ReactDOM.createPortal(
    <OVERLAY_STYLES>
      <MODAL_STYLES>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-evenly',
            height: '4vh',
          }}
        >
          <label
            className="my-auto"
            style={{ fontSize: '18px', width: '6em' }}
            htmlFor="site-search"
          >
            図書ID
          </label>
          <input
            autoFocus
            className="form-control mx-1"
            ref={inputValue}
            type="search"
            id="site-search"
            onChange={() => loadData()}
          />
          {/* <button
            className="btn btn-primary"
            type="submit"
            onClick={() => loadData()}
          >
            Search
          </button> */}
        </div>
        <RentalBookInfo
          setIsOpen={setIsOpen}
          open={isOpen}
          data={data}
          rentalUserID={rentalUserID}
        />
        <CLOSE_BUTTON onClick={close}>X</CLOSE_BUTTON>
      </MODAL_STYLES>
    </OVERLAY_STYLES>,
    document.getElementById('portal')
  )
}
