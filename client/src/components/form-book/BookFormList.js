import DatePicker from '../form-common/DatePicker'
import InputFormItem from '../form-common/InputFormItem'

// 図書情報入力フォームのリスト
export default function BookFormList({ register, errors }) {
  return (
    <div>
      {/* タイトル */}
      <InputFormItem
        itemName="タイトル"
        register={register}
        registerName="title"
        type="text"
        placeholder="JAVA入門第2版"
        errorMessage={errors?.title?.message}
      />
      {/* 著者名 */}
      <InputFormItem
        itemName="著者名"
        register={register}
        registerName="author"
        type="text"
        placeholder="中村健治"
        errorMessage={errors?.author?.message}
      />
      {/* 出版社名 */}
      <InputFormItem
        itemName="出版社名"
        register={register}
        registerName="publisher"
        type="text"
        placeholder="(株)インプレス"
        errorMessage={errors?.publisher?.message}
      />
      {/* 出版日入力ボックス */}
      <DatePicker register={register} errorMessage={errors?.pubDate?.message} />
      {/* 性別選択ラジオボタン */}
    </div>
  )
}
