import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'
import ManagerButton from '../../../components/button/ManagerButton'
import { RENTAL_BUTTON, RENTAL_TITLE } from '../../../styles/Style'

function ManagerReserve() {
  const [data, setData] = useState([])
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)

  const loadData = async () => {
    const response = await axios.get(
      'http://localhost:3001/manager/get-reservation'
    )
    setData(response.data)
  }

  useEffect(() => {
    loadData()
  }, [])

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setPage(0)
  }

  const handleBookStatus = (row) => {
    if (row === 0) {
      return '在架'
    } else {
      return '貸出中'
    }
  }

  const handleCancel = (row) => {
    const bookID = row.book_ID
    const userID = row.user_ID
    let r = window.confirm('予約を削除しますか？')
    if (!r) return
    axios.delete(
      `http://localhost:3001/manager/cancel-reserve/${bookID}/${userID}`
    )
    window.alert('完了しました。')
    setTimeout(() => {
      loadData()
    }, 0)
  }

  const handleCheck = (row) => {
    const bookID = row.book_ID
    const userID = row.user_ID
    let r = window.confirm('準備完了のメッセージを送りますか？')
    if (!r) return
    axios
      .put(`http://localhost:3001/reserve-status-change/${bookID}/${userID}`)
      .then((response) => {
        if (response.data.message) return window.alert('図書が貸出中です。')
        window.alert('完了しました。')
        setTimeout(() => {
          loadData()
        }, 0)
      })
  }

  return (
    <div style={{ marginTop: '70px' }}>
      <ManagerButton path="/manager-reserve" />
      <div style={{ width: '90vw', border: 'solid 1px #9ED2C6' }}>
        <RENTAL_TITLE>予約情報管理</RENTAL_TITLE>
        {data.length === 0 ? (
          <h6 className="text-center">予約情報がありません。</h6>
        ) : (
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                    会員ID
                  </TableCell>
                  <TableCell
                    style={{
                      color: '#2B4865',
                      fontSize: '15px',
                      width: '5.5em',
                    }}
                  >
                    図書ID
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                    タイトル
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                    著者名
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                    出版社名
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '15px' }}>
                    予約日
                  </TableCell>
                  <TableCell
                    style={{
                      color: '#2B4865',
                      fontSize: '15px',
                      width: '6.5em',
                    }}
                  >
                    図書状況
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {data.map((row, idx) => (
                  <TableRow
                    key={idx}
                    sx={{ '&:lastChild td, &:lastChild th': { border: 0 } }}
                    selected
                  >
                    <TableCell>{row.user_ID}</TableCell>
                    <TableCell>{row.book_ID}</TableCell>
                    <TableCell>{row.title}</TableCell>
                    <TableCell>{row.author}</TableCell>
                    <TableCell>{row.publisher}</TableCell>
                    <TableCell>
                      {new Date(row.reserved_date).toLocaleDateString()}
                    </TableCell>
                    <TableCell>{handleBookStatus(row.rental_flag)}</TableCell>
                    <TableCell style={{ width: '6.5em', padding: '0' }}>
                      {row.status === 0 ? (
                        <RENTAL_BUTTON onClick={() => handleCheck(row)}>
                          準備完了
                        </RENTAL_BUTTON>
                      ) : (
                        '引渡し待ち'
                      )}
                    </TableCell>
                    <TableCell>
                      <RENTAL_BUTTON onClick={() => handleCancel(row)}>
                        削除
                      </RENTAL_BUTTON>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>

            <TablePagination
              labelRowsPerPage={''}
              rowsPerPageOptions={[10, 20, 30, 40, 50]}
              component="div"
              count={data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </TableContainer>
        )}
      </div>
    </div>
  )
}

export default ManagerReserve
