import Form from 'react-bootstrap/Form'
import { StyledErrorMsg } from '../../styles/Style'

// 日付入力ボックス
export default function DatePicker({ register, errorMessage, birthday }) {
  return (
    <Form.Group className="mt-2">
      <Form.Label className="fw-bold">
        {birthday ? '生年月日' : '出版日'}
      </Form.Label>
      <div className="col-md-auto">
        <Form.Group>
          <Form.Control
            defaultValue={birthday}
            type="date"
            name="dob"
            {...register(birthday ? 'birthday' : 'pubDate')}
          />
          <StyledErrorMsg className="errors">{errorMessage}</StyledErrorMsg>
        </Form.Group>
      </div>
    </Form.Group>
  )
}

// DBから出力したデートフォーマッター　例）1999-05-17
export const formatDate = (date) => {
  let d = new Date(date)
  return (
    d.getFullYear() +
    '/' +
    (d.getMonth() + 1 > 9
      ? (d.getMonth() + 1).toString()
      : '0' + (d.getMonth() + 1)) +
    '/' +
    (d.getDate() > 9 ? d.getDate().toString() : '0' + d.getDate().toString())
  )
}
