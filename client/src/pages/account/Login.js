//Import Style
import {
  StyledFormArea,
  StyledFormButton,
  StyledTitle,
  ButtonGroup,
  ExtraText,
  TextLink,
  CopyrightText,
} from '../../styles/Style'

//Import formik
import { Formik, Form } from 'formik' //Formを管理するため
import { IdPwForm } from '../../components/form-user/IdPwForm'

//Import yup
import * as Yup from 'yup' //データを検証するため

//Import Icon
import { FiLock } from 'react-icons/fi'
import { BsFillFilePersonFill } from 'react-icons/bs'

// Import useNavigate
import { useNavigate } from 'react-router-dom' // ほかのページに遷移するため
import axios from 'axios'
import { useStore } from '../../App'
import LoginMetas from '../../metadatas'

const Login = () => {
  const navigate = useNavigate()
  return (
    <div>
      <LoginMetas />
      <StyledFormArea style={{ border: 'solid #66BFBF' }}>
        <StyledTitle style={{ size: 30 }}>図書館会員ログイン</StyledTitle>
        <Formik
          initialValues={{
            id: '',
            password: '',
          }}
          validationSchema={Yup.object({
            id: Yup.number()
              .typeError('数字のみ入力してください。')
              .min(1000000000, '10桁を入力してください。')
              .max(9999999999, '10桁を入力してください。')
              .required('IDを入力してください。')
              .integer('正しいIDを入力してください。'),
            password: Yup.string() //.min(8,"8文字以上を入力してください。")
              // .max(12,"12文字以下を入力してください。")
              .required('パスワードを入力してください。')
              .matches('^([a-z A-Z 0-9]+)$', '英数字のみ入力してください。'),
          })}
          onSubmit={(values) => {
            const { id, password } = values
            axios
              .post('http://localhost:3001/login', {
                id: id,
                password: password,
              })
              .then((response) => {
                if (response.data.message) {
                  window.alert('IDまたはパスワードが間違っています。')
                } else {
                  useStore.setState({ loginID: id })
                  setTimeout(() => {
                    navigate('/user')
                  }, 0)
                }
              })
          }}
        >
          <Form>
            <IdPwForm
              name="id"
              type="text"
              label="ログインID"
              placeholder="1234567890"
              icon={<BsFillFilePersonFill className="mt-3 ms-2" />}
            />
            <IdPwForm
              name="password"
              type="password"
              label="パスワード"
              placeholder="********"
              icon={<FiLock className="mt-3 ms-2" />}
            />
            <ButtonGroup>
              <StyledFormButton type="submit">ログイン</StyledFormButton>
            </ButtonGroup>
          </Form>
        </Formik>
        <ExtraText
          style={{
            padding: 10,
            fontSize: 10,
            textAlign: 'right',
          }}
        >
          <TextLink to="/check-user">パスワードを忘れた場合</TextLink>
          <br />
          <TextLink to="/send-mail">ログインIDを忘れた場合</TextLink>
        </ExtraText>
      </StyledFormArea>
      <CopyrightText className="mt-1">
        All rights reserved &copy;2022
      </CopyrightText>
    </div>
  )
}

export default Login
