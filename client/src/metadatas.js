import React from 'react'
import { Helmet } from 'react-helmet-async'

function LoginMetas() {
  return (
    <Helmet>
      <meta property="og:site_name" content="MG図書館" />
      <meta property="og:title" content="ログイン" />
      <meta property="og:url" content="http://localhost:3000/" />
      <meta property="og:description" content="説明" />
    </Helmet>
  )
}

export default LoginMetas
