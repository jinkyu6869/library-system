import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

// 図書情報登録確認モーダル
export default function BookConfirmModal({
  modalName,
  show,
  handleClose,
  title,
  author,
  publisher,
  pubDate,
  func,
  active,
  handleInactive,
}) {
  // モーダル表示内容
  const items = {
    タイトル: title,
    著者名: author,
    出版社名: publisher,
    // 2022-01-01 ➔ 2022/01/01
    出版日: pubDate,
  }
  //入力情報の確認内容
  const itemList = (items) => {
    // itemsオブジェクトのキー配列
    const itemsKeys = Object.keys(items)
    // itemsオブジェクトのバリュー配列
    const itemsValues = Object.values(items)
    // リストを格納するための空配列を生成
    const result = []
    // for文を回してHTMLコード生成、result配列に追加
    for (let i = 0; i < itemsKeys.length; i++) {
      result.push(
        <Row key={i} className="mb-2">
          <Col sm={4} className="text-end text-secondary">
            {itemsKeys[i]}
          </Col>
          <Col sm={6}>{itemsValues[i]} </Col>
        </Row>
      )
    }
    // result配列をリターン
    return result
  }
  return (
    <Modal
      // モーダルのプロパティ
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
    >
      {/* モーダルを閉じるボタン */}
      <Modal.Header closeButton>
        <Modal.Title>{modalName}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {/* モーダルの中身 */}
        {itemList(items)}
      </Modal.Body>
      <Modal.Footer>
        {/* 戻るボタン。押下時モーダルを閉じる。 */}
        <Button variant="outline-secondary" onClick={handleClose}>
          戻る
        </Button>
        {/* 確定ボタン */}
        <Button
          disabled={active}
          variant="primary"
          onClick={() => {
            // ボタン非活性化
            handleInactive()
            // 情報登録の関数
            func()
          }}
        >
          確定
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
