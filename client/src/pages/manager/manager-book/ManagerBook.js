import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { StyledTitle } from '../../../styles/Style'
import IconButton from '@mui/material/IconButton'
import DeleteIcon from '@mui/icons-material/Delete'
import { Table } from 'antd'
import 'antd/dist/antd.min.css'
import { useNavigate } from 'react-router-dom'
import ManagerButton from '../../../components/button/ManagerButton'

// 図書検索・予約画面
export default function SearchBooks() {
  // 検索キーワード
  const [keyword, setKeyword] = useState('')
  // 検索分類（タイトル、著者名、出版社名）
  const [category, setCategory] = useState('title')
  // 検索結果
  const [searchData, setSearchData] = useState([])
  // テーブルのローディングState
  const [loading, setLoading] = useState(false)
  const navigate = useNavigate()

  const getBooks = async (e) => {
    // テーブルをローディング中にセット
    setLoading(true)
    const response = await axios.get('http://localhost:3001/manager/get-books')
    // 検索結果がある場合、データをセット。ローディングを解除
    setSearchData(response.data)
    // テーブルのローディング状態を解除
    setLoading(false)
  }

  useEffect(() => {
    getBooks()
  }, [])

  // 図書情報を検索する関数
  const searchBooks = async (e) => {
    e.preventDefault()
    // 検索キーワードが2文字以下の場合エラーメッセージを表示
    if (keyword.length < 2) {
      alert('2文字以上入力してください。')
      return
    }
    // テーブルをローディング中にセット
    setLoading(true)
    // 選択したカテゴリーと、入力したキーワードをもって検査結果をAPI要請
    const response = await axios.get(
      `http://localhost:3001/user/search-books/${category}/${keyword}`
    )
    // response.data.length === 0
    !response.data.length
      ? // 検索結果がない場合、「検索結果がありません」と表示。ローディングを解除。
        alert('検索結果がありません。')
      : // 検索結果がある場合、データをセット。ローディングを解除
        setSearchData(response.data)
    // テーブルのローディング状態を解除
    setLoading(false)
  }

  // 図書を予約する関数。パラメータで図書のIDを取る。
  const deleteBook = async (bookID) => {
    // 予約のテーブル（reservation）に図書のIDとユーザーIDを入力
    const response = await axios.delete(
      `http://localhost:3001/manager/remove-book/${bookID}`
    )
    if (response.data.message === 'deleted') {
      alert('削除完了')
      setTimeout(() => {
        getBooks()
      }, 0)
    } else if (response.data.message === 'in use') {
      alert('図書が貸出中か予約中であるため、削除することができません。')
    } else {
      alert('DBエラー。担当者までお問い合わせください。')
    }
  }

  // テーブルのカラム設定
  const columns = [
    {
      key: '1',
      title: 'ID',
      dataIndex: 'book_ID',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.book_ID > record2.book_ID
      },
    },
    {
      key: '2',
      title: 'タイトル',
      dataIndex: 'title',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.title > record2.title
      },
      // タイトルのデザイン変更
      render: (title) => {
        return (
          <h6 className="my-auto">
            <strong>{title}</strong>
          </h6>
        )
      },
    },
    {
      key: '3',
      title: '著者名',
      dataIndex: 'author',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.author > record2.author
      },
    },
    {
      key: '4',
      title: '出版社名',
      dataIndex: 'publisher',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.publisher > record2.publisher
      },
    },
    {
      key: '5',
      title: '出版日',
      dataIndex: 'pday',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.pub_date > record2.pub_date
      },
    },
    {
      key: '6',
      title: '状態',
      dataIndex: 'book_status',
      // 図書状態コードが０の場合「在架」、１の場合「貸出中」と表示
      // フィルター機能
      filters: [
        { text: '在架', value: '在架' },
        { text: '貸出中', value: '貸出中' },
        { text: '予約中', value: '予約中' },
      ],
      onFilter: (value, record) => {
        return record.book_status === value
      },
      render: (book_status) => {
        return (
          <p className="my-auto" style={{ width: '45px' }}>
            {book_status}
          </p>
        )
      },
    },
    {
      key: '7',
      title: '予約',
      dataIndex: 'reserve_count',
      render: (reserve_count) => {
        return (
          <p className="my-auto" style={{ width: '30px' }}>
            {reserve_count} 件
          </p>
        )
      },
    },
    {
      key: '8',
      title: '',
      dataIndex: 'book_ID',
      render: (value) => {
        return (
          <IconButton
            style={{ padding: '0' }}
            aria-label="delete"
            onClick={() => {
              if (window.confirm('削除しますか？')) {
                // book IDを引数に、図書予約関数呼び出し
                deleteBook(value)
              }
            }}
          >
            <DeleteIcon />
          </IconButton>
        )
      },
    },
  ]

  return (
    <div>
      <ManagerButton path="/manager-book" />
      <StyledTitle className="mt-4" size="50">
        図書情報管理
      </StyledTitle>

      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <h6 style={{ marginTop: '3em', marginLeft: '1em' }}>
          検索結果： <strong>{searchData.length}</strong> 件
        </h6>
        {/* 検索フォーム */}
        <form
          onSubmit={searchBooks}
          className="d-flex mt-4"
          style={{ height: '40px', marginRight: '3em' }}
        >
          {/* 分類選択 */}
          <select
            className="me-2 form-select"
            style={{ width: '7.5rem', borderRadius: 0 }}
            onChange={(e) => {
              setCategory(e.target.value)
            }}
            defaultValue="title"
          >
            <option value="title">タイトル</option>
            <option value="author">著者名</option>
            <option value="publisher">出版社名</option>
            <option value="book_ID">図書ID</option>
          </select>
          {/* 検索バー */}
          <input
            className="border-secondary form-control me-2"
            type="search"
            aria-label="Search"
            style={{ width: '400px' }}
            value={keyword}
            placeholder="キーワードを入力"
            onChange={(e) => {
              setKeyword(e.target.value)
            }}
          />
        </form>
        <button
          style={{ fontSize: '18px', height: '2.5em' }}
          className="mt-4 btn btn-primary"
          variant="contained"
          onClick={() => navigate('/register-book')}
          type="button"
        >
          新規登録
        </button>
        {/* <Button
          style={{ fontSize: '18px' }}
          className="mt-4 mx-2"
          variant="contained"
          onClick={() => navigate('/register-book')}
        >
          新規登録
        </Button> */}
      </div>
      {/* 初期画面ではテーブルを表示しない */}
      <Table
        style={{ width: '90vw' }}
        className="mt-2"
        loading={loading}
        columns={columns}
        dataSource={searchData}
        rowKey="book_ID"
        pagination={{ position: ['bottomCenter'] }}
      />
    </div>
  )
}
