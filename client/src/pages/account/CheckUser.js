import React from 'react'

//Import Style
import {
  StyledFormArea,
  StyledFormButton,
  StyledTitle,
  ButtonGroup,
  CopyrightText,
} from '../../styles/Style'

//Import formik
import { Formik, Form } from 'formik'
import { IdPwForm } from '../../components/form-user/IdPwForm'

//Import yup
import * as Yup from 'yup'

//Import Icon
import { FiMail } from 'react-icons/fi'
import { BsFillFilePersonFill } from 'react-icons/bs'

// Import useNavigate
import { useNavigate } from 'react-router-dom' // ほかのページに遷移するため
import axios from 'axios'

const CheckUser = () => {
  const navigate = useNavigate()

  return (
    <div>
      <StyledFormArea style={{ border: 'solid #66BFBF' }}>
        <StyledTitle style={{ size: 30 }}>会員情報確認</StyledTitle>
        <Formik
          initialValues={{
            id: '',
            email: '',
          }}
          validationSchema={Yup.object({
            id: Yup.number()
              .typeError('数字のみ入力してください。')
              .required('IDを入力してください。')
              .integer('正しいIDを入力してください。')
              .min(1000000000, '10桁を入力してください。')
              .max(9999999999, '10桁を入力してください。'),
            email: Yup.string()
              .email('正しいメールアドレスを入力してください。')
              .max(50, '50文字以下を入力してください。')
              .required('メールアドレスを入力してください。'),
          })}
          onSubmit={(values, { setSubmitting }) => {
            const { id, email } = values
            console.log('id:', id, 'email:', email) //Check value of id, password
            axios
              .post('http://localhost:3001/check-user', {
                id: id,
                email: email,
              })
              .then((response) => {
                console.log(response.data || 'null')
                if (response.data.message) {
                  window.alert('IDまたはメールアドレスが間違っています。')
                } else {
                  setTimeout(() => {
                    navigate('/change-password')
                  }, 0)
                }
              })
          }}
        >
          <Form noValidate>
            <IdPwForm
              name="id"
              type="text"
              label="ログインID"
              placeholder="1234567890"
              icon={<BsFillFilePersonFill className="mt-3" />}
            />

            <IdPwForm
              name="email"
              type="email"
              label="メール"
              placeholder=""
              icon={<FiMail className="mt-3" />}
            />

            <ButtonGroup>
              <StyledFormButton type="submit">送信</StyledFormButton>
            </ButtonGroup>
          </Form>
        </Formik>
      </StyledFormArea>
      <CopyrightText>All rights reserved &copy;2022</CopyrightText>
    </div>
  )
}

export default CheckUser
