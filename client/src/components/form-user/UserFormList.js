import DatePicker from '../form-common/DatePicker'
import GenderRadioButtons from './GenderRadioButtons'
import InputFormItem from '../form-common//InputFormItem'

// ユーザーフォームのリスト
export default function UserFormList({
  register,
  errors,
  name,
  nameKana,
  birthday,
  gender,
  email,
  phone,
  postCode,
  address,
}) {
  return (
    <div>
      <InputFormItem
        itemName="名前（漢字）"
        defaultValue={name}
        register={register}
        registerName="name"
        type="text"
        placeholder="図書タロウ"
        errorMessage={errors?.name?.message}
      />
      <InputFormItem
        itemName="名前（カタカナ）"
        defaultValue={nameKana}
        register={register}
        registerName="nameKana"
        type="text"
        placeholder="トショタロウ"
        errorMessage={errors?.nameKana?.message}
      />
      {/* 生年月日入力ボックス */}
      <DatePicker
        register={register}
        errorMessage={errors?.birthday?.message}
        birthday={birthday}
      />
      {/* 性別選択ラジオボタン */}
      <GenderRadioButtons register={register} gender={gender} />
      <InputFormItem
        itemName="メールアドレス"
        defaultValue={email}
        register={register}
        registerName="email"
        type="email"
        placeholder="mirine@global.com"
        errorMessage={errors?.email?.message}
      />
      <InputFormItem
        itemName="電話番号"
        defaultValue={phone}
        register={register}
        registerName="phone"
        type="number"
        placeholder="09012345678"
        errorMessage={errors?.phone?.message}
      />
      <InputFormItem
        itemName="郵便番号"
        defaultValue={postCode}
        register={register}
        registerName="postCode"
        type="number"
        placeholder="1204577"
        errorMessage={errors?.postCode?.message}
      />
      <InputFormItem
        itemName="住所"
        defaultValue={address}
        register={register}
        registerName="address"
        type="text"
        placeholder="東京都豊島区駒込１ー２ー３ミリネビル２０１"
        errorMessage={errors?.address?.message}
      />
    </div>
  )
}
