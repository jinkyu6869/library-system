import React, { useState, useEffect } from 'react'
import axios from 'axios'
import BootstrapTable from 'react-bootstrap-table-next'
import { FaArrowsAltV } from 'react-icons/fa'

import { StyledFormButton, CopyrightText } from '../../../styles/Style'
import { useNavigate } from 'react-router-dom'

import paginationFactory from 'react-bootstrap-table2-paginator'
import cellEditFactory from 'react-bootstrap-table2-editor'
import ToolkitProvider, {
  Search,
} from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit'
import { formatDate } from '../../../components/form-common/DatePicker'
import ManagerButton from '../../../components/button/ManagerButton'
import { Button } from '@mui/material'

function ManagerUser() {
  let id = []
  const [data, setData] = useState([])
  const [showFlag, setShowFlag] = useState(0)
  const [loginID, setLoginID] = useState()

  const loadID = async () => {
    const response = await axios.get('http://localhost:3001/get-login-id')
    setLoginID(response.data.data)
  }
  const loadData = async () => {
    const response = await axios.get(
      `http://localhost:3001/manager/get-users/${showFlag}`
    )
    setData(response.data)
  }

  useEffect(() => {
    loadData()
    loadID()
    // eslint-disable-next-line
  }, [showFlag])

  const selectRow = {
    mode: 'checkbox',
    clickToSelect: true,
    bgColor: '#aaa',
    selected: [],
    onSelectAll: (isSelect, row) => {
      if (id.length === 0) {
        for (let i = 0; i < row.length; i++) {
          id.push(row[i].id)
        }
      } else {
        id = []
      }
    },
    onSelect: (row) => {
      if (id.length === 0) {
        id.push(row.id)
      } else {
        if (id.indexOf(row.id) === -1) {
          id.push(row.id)
        } else {
          id.splice(id.indexOf(row.id), 1)
        }
      }
    },
  }

  const upDownFormatter = (column, colIndex) => {
    return (
      <span>
        <FaArrowsAltV />
        {column.text}
      </span>
    )
  }

  const dayFormatter = (data, row) => {
    const d = data
    let day = new Date(d)
    return <span>{formatDate(day).replaceAll('-', '/')}</span>
  }

  const phoneFormatter = (data, row) => {
    const d = data
    return (
      <span>
        {d.substr(0, 3) + '-' + d.substr(3, 4) + '-' + d.substr(7, 4)}
      </span>
    )
  }

  const genderFormatter = (data, row) => {
    const d = data
    if (d === 'm') {
      return <span>男性</span>
    } else {
      return <span>女性</span>
    }
  }

  const { SearchBar } = Search
  const columns = [
    {
      dataField: 'id',
      text: 'ID',
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'name',
      text: '名前(漢字)',
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'name_kana',
      text: '名前(カナ)',
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'birthday',
      text: '生年月日',
      sort: true,
      formatter: dayFormatter,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'email',
      text: 'メールアドレス',
      sort: true,
    },
    {
      dataField: 'phone',
      text: '電話番号',
      formatter: phoneFormatter,
    },
    {
      dataField: 'gender',
      text: '性別',
      formatter: genderFormatter,
    },
    {
      dataField: 'reg_ID',
      text: '登録者ID',
    },
    {
      dataField: 'reg_date',
      text: '登録日',
      formatter: dayFormatter,
      headerFormatter: upDownFormatter,
    },
  ]

  const deleteUser = (id) => {
    let checkLogicIdDelete = id.filter((value) => {
      return value === parseInt(loginID)
    })
    if (id.length < 1) {
      window.alert('1つ以上の項目を選択してください。')
    } else if (checkLogicIdDelete.length > 0) {
      window.alert('ログイン中のIDは削除できません。')
    } else {
      if (
        window.confirm(
          `選択した ${id.length} 個の会員情報を本当に削除しますか? 後で戻すことはできません。`
        )
      ) {
        axios.delete(`http://localhost:3001/manager/remove-users/${id}`)
        window.alert('削除が完了しました。')
        setTimeout(() => {
          loadData()
        }, 0)
      }
    }
  }

  const toggleHideUsers = (id) => {
    let checkLogicId = id.filter((value) => {
      return value === parseInt(loginID)
    })
    if (id.length < 1) {
      window.alert('1つ以上の項目を選択してください。')
    } else if (checkLogicId.length > 0) {
      window.alert('ログイン中のIDは非表示にできません。')
    } else {
      if (
        window.confirm(
          `選択した ${id.length} 個の会員情報を${
            !showFlag ? '非表示に' : '表示'
          }しますか?`
        )
      ) {
        axios.delete(
          `http://localhost:3001/manager/toggle-hide-users/${id}/${showFlag}`
        )
        window.alert('完了しました。')
        setTimeout(() => {
          loadData()
        }, 0)
      }
    }
  }

  const navigate = useNavigate()

  //Send value of selected ID to edit page
  const toEditPage = () => {
    if (id.length !== 1) {
      window.alert('1つの項目を選択してください。')
    } else {
      navigate('/edit-user', { state: { id: id } })
    }
  }

  return (
    <div className="justify-center">
      <h1
        className="mx-auto flex 
            flex col justify-center items-center"
        style={{
          textAlign: 'center',
          fontSize: '4.0em',
          padding: '50px',
        }}
      >
        会員情報管理
      </h1>
      <ManagerButton path="/manager-user" />

      <div
        style={{
          display: 'flex',
          justifyContent: 'space-evenly',
        }}
      >
        <StyledFormButton
          onClick={() => {
            toEditPage()
          }}
        >
          修正
        </StyledFormButton>
        <StyledFormButton onClick={() => deleteUser(id)}>削除</StyledFormButton>
        <StyledFormButton onClick={() => toggleHideUsers(id)}>
          {!showFlag ? '非表示' : '表示'}
        </StyledFormButton>
        <StyledFormButton onClick={() => navigate('/register-user')}>
          新規登録
        </StyledFormButton>
      </div>
      <div
        style={{
          width: '1100px',
        }}
      >
        {
          <ToolkitProvider keyField="id" data={data} columns={columns} search>
            {(props) => (
              <div>
                <SearchBar
                  placeholder="ID、名前…"
                  srText="会員情報検索"
                  delay="700"
                  {...props.searchProps}
                />
                <hr />
                <div>
                  <Button
                    className="mb-3"
                    style={{
                      height: '40px',
                    }}
                    variant="contained"
                    onClick={() => setShowFlag(!showFlag ? 1 : 0)}
                  >
                    {!showFlag
                      ? '非表示会員情報リストを見る'
                      : '表示会員情報リストを見る'}
                  </Button>
                  {!showFlag ? (
                    <span></span>
                  ) : (
                    <span className="mx-2 text-danger">
                      *非表示に設定した会員情報リストです。操作にご注意ください。
                    </span>
                  )}
                </div>
                <BootstrapTable
                  className="bs_table"
                  {...props.baseProps}
                  striped
                  hover
                  condensed
                  pagination={paginationFactory()}
                  // paginationHAlign="right"
                  cellEdit={cellEditFactory({})}
                  selectRow={selectRow}
                />
              </div>
            )}
          </ToolkitProvider>
        }
        <CopyrightText>All rights reserved &copy;2022</CopyrightText>
      </div>
    </div>
  )
}

export default ManagerUser
