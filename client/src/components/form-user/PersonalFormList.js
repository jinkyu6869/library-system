import PersonalFormItem from './PersonalFormItem'
import InputFormItem from '../form-common/InputFormItem'

// 個人情報修正フォームのリスト
export default function PersonalFormList({
  register,
  errors,
  name,
  nameKana,
  birthday,
  gender,
  email,
  phone,
  postCode,
  address,
}) {
  return (
    <div>
      <PersonalFormItem itemName="名前（漢字）" defaultValue={name} />
      <PersonalFormItem itemName="名前（カタカナ）" defaultValue={nameKana} />
      <PersonalFormItem
        itemName="生年月日"
        defaultValue={birthday.replaceAll('-', '/')}
      />
      <PersonalFormItem
        itemName="性別"
        defaultValue={gender === 'm' ? '男性' : '女性'}
      />
      <InputFormItem
        itemName="メールアドレス"
        defaultValue={email}
        register={register}
        registerName="email"
        type="email"
        placeholder="mirine@global.com"
        errorMessage={errors?.email?.message}
      />
      <InputFormItem
        itemName="電話番号"
        defaultValue={phone}
        register={register}
        registerName="phone"
        type="number"
        placeholder="09012345678"
        errorMessage={errors?.phone?.message}
      />
      <InputFormItem
        itemName="郵便番号"
        defaultValue={postCode}
        register={register}
        registerName="postCode"
        type="number"
        placeholder="1256817"
        errorMessage={errors?.postCode?.message}
      />
      <InputFormItem
        itemName="住所"
        defaultValue={address}
        register={register}
        registerName="address"
        type="text"
        placeholder="東京都豊島区駒込１ー２ー３ミリネビル２０１"
        errorMessage={errors?.address?.message}
      />
    </div>
  )
}
