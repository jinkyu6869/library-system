import axios from 'axios'
import { useEffect, useState } from 'react'
import { Route, Routes } from 'react-router-dom'
import ChangePassword from '../../pages/account/ChangePassword'
import CheckUser from '../../pages/account/CheckUser'
import Login from '../../pages/account/Login'
import SendMail from '../../pages/account/SendMail'
import Error from '../../pages/etc/Error'
import EditUser from '../../pages/manager/manager-user/EditUser'
import ManagerUser from '../../pages/manager/manager-user/ManagerUser'
import RegisterUser from '../../pages/manager/manager-user/RegisterUser'
import User from '../../pages/user/User'
import SearchBooks from '../../pages/user/SearchBooks'
import { StyledContainer } from '../../styles/Style'
import ProtectedRoutes from '../login-auth/ProtectedRoutes'
import EditPersonal from '../../pages/user/EditPersonal'
import ManagerRental from '../../pages/manager/manager-rental/ManagerRental'
import ManagerReserve from '../../pages/manager/manager-reserve/ManagerReserve'
import UserRental from '../../pages/user/user-rental/UserRental'
import UserReserve from '../../pages/user/user-reserve/UserReserve'
import ManagerBook from '../../pages/manager/manager-book/ManagerBook'
import RegisterBook from '../../pages/manager/manager-book/RegisterBook'

function Root() {
  const [authorityCODE, setAuthorityCODE] = useState(2)
  const loadCode = async () => {
    const response = await axios.get('http://localhost:3001/get-authority-code')
    setAuthorityCODE(response.data.data)
  }
  useEffect(() => {
    loadCode()
  }, [])

  let isLogged = authorityCODE === 2 ? false : true

  return (
    <div>
      <StyledContainer>
        <Routes>
          {/* アカウント画面 */}
          <Route index path="/" element={<Login />} />
          <Route path="send-mail" element={<SendMail />} />
          <Route path="check-user" element={<CheckUser />} />
          <Route path="change-password" element={<ChangePassword />} />
          {/* <Route element={<ProtectedRoutes isLogged={isLogged} />}> */}
          {/* ユーザー画面 */}
          <Route path="user" element={<User />}></Route>
          <Route path="search-books" element={<SearchBooks />} />
          <Route path="edit-personal" element={<EditPersonal />} />
          <Route path="user-rental" element={<UserRental />} />
          <Route path="user-reserve" element={<UserReserve />} />
          {/* 管理者画面（ユーザー） */}
          <Route path="manager-user" element={<ManagerUser />} />
          <Route path="register-user" element={<RegisterUser />} />
          <Route path="edit-user" element={<EditUser />} />
          <Route path="error" element={<Error />} />
          {/* 管理者画面（図書） */}
          <Route path="manager-reserve" element={<ManagerReserve />} />
          <Route path="manager-rental" element={<ManagerRental />} />
          <Route path="manager-book" element={<ManagerBook />} />
          <Route path="register-book" element={<RegisterBook />} />
          {/* </Route> */}
        </Routes>
      </StyledContainer>
    </div>
  )
}

export default Root
