import axios from 'axios'
import React, { useState } from 'react'
import { formatDate } from '../../components/form-common/DatePicker'
import { StyledTitle } from '../../styles/Style'
import { Table } from 'antd'
import 'antd/dist/antd.min.css'
import { useStore } from '../../App'
import ToMyLibraryButton from '../../components/button/ToMyLibraryButton'

// 図書検索・予約画面
export default function SearchBooks() {
  const { loginID } = useStore()
  // 検索キーワード
  const [keyword, setKeyword] = useState('')
  // 検索分類（タイトル、著者名、出版社名）
  const [category, setCategory] = useState('title')
  // 検索結果
  const [searchData, setSearchData] = useState([])
  // テーブルのローディングState
  const [loading, setLoading] = useState(false)
  // 図書情報を検索する関数
  const searchBooks = async (e) => {
    e?.preventDefault()
    // 検索キーワードが2文字以下の場合エラーメッセージを表示
    if (keyword.length < 2) {
      alert('2文字以上入力してください。')
      return
    }
    // テーブルをローディング中にセット
    setLoading(true)
    // 選択したカテゴリーと、入力したキーワードをもって検査結果をAPI要請
    const response = await axios.get(
      `http://localhost:3001/user/search-books/${category}/${keyword}`
    )
    !response.data.length
      ? // 検索結果がない場合、「検索結果がありません」と表示。ローディングを解除。
        alert('検索結果がありません。')
      : // 検索結果がある場合、データをセット。ローディングを解除
        setSearchData(response.data)
    // テーブルのローディング状態を解除
    setLoading(false)
  }

  // 図書を予約する関数。パラメータで図書のIDを取る。
  const reserveBook = async (bookID) => {
    // 予約のテーブル（reservation）に図書のIDとユーザーIDを入力
    const response = await axios.post(
      `http://localhost:3001/user/reserve-book/${bookID}/${loginID}`
    )
    const message = response.data.message
    if (message === 'inserted') {
      alert('予約完了')
      setTimeout(() => {
        searchBooks()
      }, 0)
    } else if (
      // 同じ本を予約しようとした場合
      message === 'dup entry'
    ) {
      alert('すでに予約しています。')
    } else if (
      // 3冊を超過して予約しようとした場合
      message === 'reserve limit'
    ) {
      alert('3冊を超過して予約することはできません。')
    } else if (message === 'rental-ing') {
      alert('貸出中の図書は予約できません。')
    } else {
      alert('DBエラー。担当者までお問い合わせください。')
    }
  }

  // テーブルのカラム設定
  const columns = [
    {
      key: '1',
      title: '図書ID',
      dataIndex: 'book_ID',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.book_ID > record2.book_ID
      },
      render: (book_ID) => {
        return (
          <p className="my-auto" style={{ width: '5em' }}>
            {book_ID}
          </p>
        )
      },
    },
    {
      key: '2',
      title: 'タイトル',
      dataIndex: 'title',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.title > record2.title
      },
      // タイトルのデザイン変更
      render: (title) => {
        return (
          <h6 className="my-auto">
            <strong>{title}</strong>
          </h6>
        )
      },
    },
    {
      key: '3',
      title: '著者名',
      dataIndex: 'author',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.author > record2.author
      },
    },
    {
      key: '4',
      title: '出版社名',
      dataIndex: 'publisher',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.publisher > record2.publisher
      },
    },
    {
      key: '5',
      title: '出版日',
      dataIndex: 'pub_date',
      // 並び替え機能
      sorter: (record1, record2) => {
        return record1.pub_date > record2.pub_date
      },
      // 出版日の表示 ➔ 2020/01/28
      render: (pub_date) => {
        return (
          <div>
            <p className="my-auto">
              {formatDate(pub_date).replaceAll('-', '/')}
            </p>
          </div>
        )
      },
    },
    {
      key: '6',
      title: '状態',
      dataIndex: 'book_status',
      // 図書状態コードが０の場合「在架」、１の場合「貸出中」と表示
      // フィルター機能
      filters: [
        { text: '在架', value: '在架' },
        { text: '貸出中', value: '貸出中' },
        { text: '予約中', value: '予約中' },
      ],
      onFilter: (value, record) => {
        return record.book_status === value
      },
      render: (book_status) => {
        return (
          <p className="my-auto" style={{ width: '45px' }}>
            {book_status}
          </p>
        )
      },
    },
    {
      key: '7',
      title: '予約',
      dataIndex: 'reserve_count',
      render: (reserve_count) => {
        return (
          <p className="my-auto" style={{ width: '30px' }}>
            {reserve_count} 件
          </p>
        )
      },
    },
    // 予約ボタン
    {
      key: '8',
      title: '',
      dataIndex: 'book_ID',
      render: (value) => {
        return (
          <button
            type="button"
            className="btn btn-outline-success"
            style={{ width: '60px' }}
            onClick={() => {
              const answer = window.confirm('予約しますか？')
              if (answer) {
                // book IDを引数に、図書予約関数呼び出し
                reserveBook(value)
              }
            }}
          >
            予約
          </button>
        )
      },
    },
  ]

  return (
    <div>
      <ToMyLibraryButton />
      <StyledTitle className="mt-4" size="50">
        図書情報検索
      </StyledTitle>
      {/* 検索フォーム */}
      <form
        onSubmit={searchBooks}
        className="d-flex "
        style={{ justifyContent: 'center' }}
      >
        {/* 分類選択 */}
        <select
          className="me-2 form-select"
          style={{ width: '7.5rem', borderRadius: 0 }}
          onChange={(e) => {
            setCategory(e.target.value)
          }}
          defaultValue="title"
        >
          <option value="title">タイトル</option>
          <option value="author">著者名</option>
          <option value="publisher">出版社名</option>
          <option value="book_ID">図書ID</option>
        </select>
        {/* 検索バー */}
        <input
          className="border-secondary form-control me-2"
          type="search"
          aria-label="Search"
          style={{ width: '380px' }}
          value={keyword}
          placeholder="キーワードを入力"
          onChange={(e) => {
            setKeyword(e.target.value)
          }}
        />
        <button className="btn btn-primary" type="submit">
          検索
        </button>
      </form>
      {/* 初期画面ではテーブルを表示しない */}
      {!searchData.length || (
        <div>
          <h6 className="ms-3">
            検索結果： <strong>{searchData.length}</strong> 件
          </h6>
          <Table
            style={{ width: '90vw' }}
            className="mt-2"
            loading={loading}
            columns={columns}
            dataSource={searchData}
            rowKey="book_ID"
            pagination={{ position: ['bottomCenter'] }}
          />
        </div>
      )}
    </div>
  )
}
