import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'

import Root from './components/route/Root'
import create from 'zustand'
// グローバルステート管理
export const useStore = create(() => ({
  loginID: '',
}))

function App() {
  return (
    <Router>
      <Root />
    </Router>
  )
}
export default App
