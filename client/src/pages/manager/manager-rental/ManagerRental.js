import axios from 'axios'
import { useEffect, useState } from 'react'
import BootstrapTable from 'react-bootstrap-table-next'
import ToolkitProvider from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit'
import paginationFactory from 'react-bootstrap-table2-paginator'
import cellEditFactory from 'react-bootstrap-table2-editor'
import { CopyrightText } from '../../../styles/Style'
import RentalBookSearch from './RentalBookSearch'
import ManagerButton from '../../../components/button/ManagerButton'
import { formatDate } from '../../../components/form-common/DatePicker'

function ManagerRental() {
  const [rentalUserID, setRentalUserID] = useState()
  const [bookID, setBookID] = useState()
  const [userID, setuserID] = useState()
  const [data, setData] = useState([])
  const [isOpen, setIsOpen] = useState(false)
  const loadData = async () => {
    const response = await axios.get(
      'http://localhost:3001/manager/rental-detail'
    )
    setData(response.data)
  }

  useEffect(() => {
    loadData()
  }, [])

  const selectRow = {
    mode: 'radio',
    clickToSelect: true,
    bgColor: '#aaa',
    onSelect: (row) => {
      setBookID(row.book_ID)
      setuserID(row.user_ID)
    },
  }

  const handleStatus = (data, row) => {
    const d = data
    if (d === 0) {
      return '貸出中'
    } else {
      return '延長中'
    }
  }

  const bookIDFormatter = (data, row) => {
    return <span>{data}&nbsp;&nbsp;&nbsp;</span>
  }

  const dateFormatter = (data, row) => {
    const d = data
    let day = new Date(d)
    return <span>{formatDate(day).replaceAll('-', '/')}</span>
  }

  const returnDateFormatter = (data, row) => {
    const day = new Date(data)
    const current = new Date()
    day.setHours(0, 0, 0, 0)
    current.setHours(0, 0, 0, 0)
    if (day < current) {
      return (
        <span className="text-danger">
          {formatDate(day).replaceAll('-', '/')}
        </span>
      )
    } else {
      return <span>{formatDate(day).replaceAll('-', '/')}</span>
    }
  }

  const handleDelete = (bookID, userID) => {
    let r = window.confirm('図書を返却しますか？')
    if (r) {
      axios
        .delete(`http://localhost:3001/rental-delete/${bookID}/${userID}`)
        .then((response) => {
          if (response.data.message) {
            return window.alert(
              '図書が予約されています。予約棚に保管してください。'
            )
          }
        })
      alert('返却完了')
      setTimeout(() => {
        loadData()
      }, 0)
    }
  }

  // const { SearchBar } = Search
  const columns = [
    {
      // dataField: 'member_ID',
      dataField: 'user_ID',
      text: '会員ID',
    },
    {
      dataField: 'book_ID',
      text: '図書ID',
      formatter: bookIDFormatter,
    },
    {
      dataField: 'title',
      text: 'タイトル',
    },
    {
      dataField: 'author',
      text: '著者名',
    },
    {
      dataField: 'publisher',
      text: '出版社名',
    },
    {
      dataField: 'rent_date',
      text: '貸出日',
      formatter: dateFormatter,
    },
    {
      dataField: 'return_date',
      text: '返却日',
      formatter: returnDateFormatter,
    },
    // {
    //   dataField: 'ex_period_flag',
    //   text: '状況',
    //   formatter: handleStatus,
    // },
  ]

  const MySearch = (props) => {
    let input

    const handleChange = async () => {
      const keyword = input.value
      props.onSearch(keyword)
      if (keyword.length === 10 && !isNaN(keyword)) {
        setTimeout(() => {
          setRentalUserID(keyword)
        }, 0)
      }
    }
    return (
      <div>
        <input
          className="form-control"
          ref={(n) => (input = n)}
          type="text"
          placeholder="貸出のためには会員カードを、返却のためには図書のバーコードを読み取ってください。"
          onChange={() => {
            handleChange(props.onSearch(input.value))
          }}
        />
      </div>
    )
  }

  const checkCanRental = async () => {
    const res = await axios.get(
      `http://localhost:3001/manager/check-user/${rentalUserID}`
    )
    if (res.data.message === 'not-user') {
      alert('会員情報が見つかりません。')
    } else {
      const response = await axios.get(
        `http://localhost:3001/manager/check-can-rental/${rentalUserID}`
      )
      if (response.data.message === 'counts-over-five') {
        alert('5冊を超過して貸出できません。')
        return
      } else if (response.data.message === 'overdue') {
        alert('延滞していると貸出できません。')
        return
      } else {
        // console.log(response)
        setIsOpen(true)
      }
    }
  }

  return (
    <div>
      <ManagerButton path="/manager-rental" />
      <div className="justify-center">
        <h1
          className="mx-auto flex 
            flex col justify-center items-center"
          style={{
            textAlign: 'center',
            fontSize: '4.0em',
            padding: '50px',
          }}
        >
          貸出情報管理
        </h1>

        {isOpen && (
          <RentalBookSearch
            rentalUserID={rentalUserID}
            show={isOpen}
            close={() => {
              setIsOpen(false)
              loadData()
            }}
          />
        )}

        {data.length === 0 ? (
          <h5 className="mt-5 text-center">貸出情報がありません。</h5>
        ) : (
          <div style={{ width: '90vw' }}>
            <ToolkitProvider
              keyField="book_ID"
              data={data}
              columns={columns}
              search
            >
              {(props) => (
                <div>
                  {!rentalUserID ? (
                    <MySearch {...props.searchProps} />
                  ) : (
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <h6 className="align-middle">
                        ※会員ID {rentalUserID} の貸出を行ってください。
                      </h6>
                      <button
                        type="button"
                        className="btn btn-outline-danger btn-sm mb-1"
                        onClick={() => {
                          setRentalUserID()
                        }}
                      >
                        キャンセル
                      </button>
                    </div>
                  )}
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => checkCanRental()}
                    disabled={!rentalUserID}
                  >
                    貸出
                  </button>
                  <button
                    type="button"
                    className="btn btn-success mx-2 my-2"
                    onClick={() => {
                      handleDelete(bookID, userID)
                    }}
                    disabled={!userID}
                  >
                    返却
                  </button>
                  <BootstrapTable
                    {...props.baseProps}
                    striped
                    hover
                    condensed
                    pagination={paginationFactory()}
                    cellEdit={cellEditFactory({})}
                    selectRow={selectRow}
                  />
                </div>
              )}
            </ToolkitProvider>
            <CopyrightText>All rights reserved &copy;2022</CopyrightText>
          </div>
        )}
      </div>
    </div>
  )
}

export default ManagerRental
