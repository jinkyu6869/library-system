// ライブラリー
import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
// 自作コンポーネント
import FormFooter from '../../components/form-common/FormFooter'
import UserConfirmModal from '../../components/form-user/UserConfirmModal'
import { personalSchema } from '../../components/form-common/Validation'
import { formatDate } from '../../components/form-common/DatePicker'
import PersonalFormList from '../../components/form-user/PersonalFormList'
import { useStore } from '../../App'
import ToMyLibraryButton from '../../components/button/ToMyLibraryButton'

// 会員情報修正画面
export default function EditPersonal() {
  const { loginID } = useStore()
  const [name, setName] = useState('')
  const [nameKana, setNameKana] = useState('')
  const [birthday, setBirthday] = useState('')
  const [gender, setGender] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [postCode, setPostCode] = useState('')
  const [address, setAddress] = useState('')
  const [authorityCODE, setAuthorityCODE] = useState(0)
  // 画面遷移 hook
  const navigate = useNavigate()
  // react hook form
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(personalSchema),
  })

  // 選択した会員情報をDBから取得する関数
  const getUser = () => {
    axios.get(`http://localhost:3001/get-user/${loginID}`).then((response) => {
      const r = response.data[0]
      setName(r.name)
      setNameKana(r.name_kana)
      setBirthday(formatDate(r.birthday))
      setGender(r.gender)
      setEmail(r.email)
      setPhone(r.phone)
      setPostCode(r.postCode)
      setAddress(r.address)
      setAuthorityCODE(r.authority_CODE)
      reset()
    })
  }

  // 画面起動時１回だけログインID取得、会員情報取得
  useEffect(() => {
    getUser()
    // eslint-disable-next-line
  }, [])

  // 修正した会員情報をDBにアップデートする関数
  const updPersonal = () => {
    axios
      .put('http://localhost:3001/user/edit-personal', {
        id: loginID,
        email: email,
        phone: phone,
        postCode: postCode,
        address: address,
        updID: loginID,
      })
      .then((response) => {
        const r = response.data.message
        // DBエラーメッセージ表示
        if (r === 'updated') {
          alert('修正完了')
          navigate('/user')
        } else {
          if (r === 'dup email') {
            alert('メールアドレスが重複しています。')
          } else if (r === 'dup phone') {
            alert('電話番号が重複しています。')
          } else {
            alert(
              '再度ログインしてもできない場合は、担当者までお問い合わせください。'
            )
          }
          // モーダルを閉じる
          handleClose()
          // 確定ボタンを再活性化
          handleActive()
        }
      })
  }

  //モーダル制御
  const [show, setShow] = useState(false)
  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  //確定ボタン制御
  const [active, setActive] = useState(false)
  const handleInactive = () => setActive(true)
  const handleActive = () => setActive(false)

  // submit時の処理
  const onSubmit = (input) => {
    setEmail(input.email)
    setPhone(input.phone)
    setPostCode(input.postCode)
    setAddress(input.address)
    // モーダルを開く
    handleShow()
  }

  return (
    <div>
      <ToMyLibraryButton />
      <div className="card my-5 mx-auto" style={{ width: '25rem' }}>
        <div className="card-body">
          {/* フォームヘッダー */}
          <h4 className="mt-1">個人情報修正</h4>
          <div
            className="border-bottom mt-3 mb-2"
            style={{ margin: '-16px' }}
          ></div>
          <form
            // ブラウザーの基本バリデーション非活性化
            noValidate
            // handleSubmitでバリデーション ➔ モーダル表示
            onSubmit={handleSubmit((input) => {
              onSubmit(input)
            })}
          >
            {/* フォームリスト */}
            <PersonalFormList
              register={register}
              errors={errors}
              name={name}
              nameKana={nameKana}
              birthday={birthday}
              gender={gender}
              email={email}
              phone={phone}
              postCode={postCode}
              address={address}
            />
            {/* フォームフッター */}
            <FormFooter reset={getUser} />
          </form>
          {/* 確認モーダル */}
        </div>
      </div>
      <UserConfirmModal
        modalName="個人情報修正確認"
        show={show}
        handleClose={handleClose}
        id={loginID}
        name={name}
        nameKana={nameKana}
        birthday={birthday}
        gender={gender}
        email={email}
        phone={phone}
        postCode={postCode}
        address={address}
        authorityCODE={authorityCODE}
        // updUser: 修正関数
        func={updPersonal}
        active={active}
        handleInactive={handleInactive}
      />
    </div>
  )
}
