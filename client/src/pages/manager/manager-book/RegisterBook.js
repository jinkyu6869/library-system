// ライブラリー
import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
// 自作コンポーネント
import FormFooter from '../../../components/form-common/FormFooter'
import { bookSchema } from '../../../components/form-common/Validation'
import { useStore } from '../../../App'
import BookFormList from '../../../components/form-book/BookFormList'
import BookConfirmModal from '../../../components/form-book/BookConfirmModal'
import { formatDate } from '../../../components/form-common/DatePicker'

// 会員情報登録画面
export default function RegisterBook() {
  //取得したログインID変数
  const { loginID } = useStore()
  //入力情報変数
  const [title, setTitle] = useState('')
  const [author, setAuthor] = useState('')
  const [publisher, setPublisher] = useState('')
  const [pubDate, setPubDate] = useState('')
  // 画面遷移 hook
  const navigate = useNavigate()
  // react hook form
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(bookSchema),
  })

  //入力した会員情報をDBに登録する関数
  const addBook = () => {
    axios
      .post('http://localhost:3001/manager/register-book', {
        title: title,
        author: author,
        publisher: publisher,
        pubDate: pubDate,
        regID: loginID,
      })
      .then((response) => {
        // DBエラーメッセージ表示
        const r = response.data.message
        if (r === 'inserted') {
          alert('登録完了')
          // 管理者画面に遷移
          navigate('/manager-book')
        } else {
          alert(
            '再度ログインしもできない場合は、担当者までお問い合わせください。'
          )
          // モーダルを閉じる
          handleClose()
          // 確定ボタンを再活性化
          handleActive()
        }
      })
  }

  //モーダル制御
  const [show, setShow] = useState(false)
  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  //確定ボタン制御
  const [active, setActive] = useState(false)
  const handleInactive = () => setActive(true)
  const handleActive = () => setActive(false)

  // submit時の処理
  const onSubmit = (input) => {
    setTitle(input.title)
    setAuthor(input.author)
    setPublisher(input.publisher)
    setPubDate(formatDate(input.pubDate))
    handleShow()
  }

  return (
    <div className="card my-5 mx-auto" style={{ width: '25rem' }}>
      <div className="card-body">
        {/* フォームヘッダー */}
        <h4 className="mt-1">図書情報登録</h4>
        <div
          className="border-bottom mt-3 mb-2"
          style={{ margin: '-16px' }}
        ></div>
        <form
          // ブラウザーの基本バリデーション非活性化
          noValidate
          // handleSubmitでバリデーション ➔ モーダル表示
          onSubmit={handleSubmit((input) => {
            onSubmit(input)
          })}
        >
          {/* フォームリスト */}
          <BookFormList register={register} errors={errors} />
          {/* フォームフッター */}
          <FormFooter
            reset={reset}
            navigate={() => navigate('/manager-book')}
          />
        </form>
        {/* 確認モーダル */}
      </div>
      <BookConfirmModal
        modalName="図書情報登録確認"
        show={show}
        handleClose={handleClose}
        title={title}
        author={author}
        publisher={publisher}
        pubDate={pubDate}
        func={addBook}
        active={active}
        handleInactive={handleInactive}
      />
    </div>
  )
}
