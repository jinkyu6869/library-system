import React from 'react'
import ReactDOM from 'react-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTriangleExclamation } from '@fortawesome/free-solid-svg-icons'
import {
  CANCEL_BUTTON,
  CLOSE_BUTTON,
  LINE,
  MODAL_STYLES,
  OVERLAY_STYLES,
  TABLE,
  TABLE_TD,
} from '../../../styles/Style'
export default function CancelReserve({
  title,
  author,
  publisher,
  show,
  close,
  cancel,
}) {
  if (!show) return null
  return ReactDOM.createPortal(
    <OVERLAY_STYLES>
      <MODAL_STYLES>
        <div>
          <h3 style={{ marginBottom: '2rem' }}>
            <FontAwesomeIcon icon={faTriangleExclamation} /> 予約キャンセル確認
          </h3>
          <LINE />
          <div>
            <h5>図書詳細：</h5>
            <TABLE>
              <tbody>
                <tr>
                  <TABLE_TD>タイトル</TABLE_TD>
                  <TABLE_TD>{title}</TABLE_TD>
                </tr>
                <tr>
                  <TABLE_TD>著者名</TABLE_TD>
                  <TABLE_TD>{author}</TABLE_TD>
                </tr>
                <tr>
                  <TABLE_TD>出版社</TABLE_TD>
                  <TABLE_TD>{publisher}</TABLE_TD>
                </tr>
              </tbody>
            </TABLE>
          </div>
          <CANCEL_BUTTON onClick={cancel}>予約キャンセル</CANCEL_BUTTON>
        </div>
        <CLOSE_BUTTON onClick={close}>X</CLOSE_BUTTON>
      </MODAL_STYLES>
    </OVERLAY_STYLES>,
    document.getElementById('portal')
  )
}
