import * as Yup from 'yup'
import 'yup-phone'

// 会員情報フォームのバリデーション
export const userSchema = Yup.object().shape({
  name: Yup.string()
    .max(50, '50文字以下を入力してください。')
    .required('名前（漢字）を入力してください。')
    .matches(/^[^\x20-\x7e]*$/, '全角文字のみ'),
  nameKana: Yup.string()
    .max(50, '50文字以下を入力してください。')
    .required('名前（カタカナ）を入力してください。')
    .matches(/^[ァ-ー]+$/, '全角カタカナのみ'),
  birthday: Yup.date()
    .typeError('正しい日付を入力してください。')
    .required('生年月日を入力してください。'),
  email: Yup.string()
    .max(50, '50文字以下を入力してください。')
    .email('正しいメールアドレスを入力してください。')
    .required('メールアドレスを入力してください。'),
  phone: Yup.string()
    .phone('JP', true, '正しい電話番号を入力してください。')
    .required('電話番号を入力してください。'),
  postCode: Yup.string()
    .matches(/^\d{7}$/, '正しい郵便番号を入力してください。')
    .required('郵便番号を入力してください。'),
  address: Yup.string()
    .required('住所を入力してください。')
    .max(70, '70文字以下を入力してください。')
    .matches(/^[^\x20-\x7e]*$/, '全角文字のみ'),
})
// 個人情報フォームのバリデーション
// 未完成
export const personalSchema = Yup.object().shape({
  email: Yup.string()
    .max(50, '50文字以下を入力してください。')
    .email('正しいメールアドレスを入力してください。')
    .required('メールアドレスを入力してください。'),
  phone: Yup.string()
    .phone('JP', true, '正しい電話番号を入力してください。')
    .required('電話番号を入力してください。'),
  postCode: Yup.string()
    .matches(/^\d{7}$/, '正しい郵便番号を入力してください。')
    .required('郵便番号を入力してください。'),
  address: Yup.string()
    .required('住所を入力してください。')
    .max(70, '70文字以下を入力してください。')
    .matches(/^[^\x20-\x7e]*$/, '全角文字のみ'),
})
// 図書情報フォームのバリデーション
export const bookSchema = Yup.object().shape({
  title: Yup.string()
    .max(50, '50文字以下を入力してください。')
    .required('タイトルを入力してください。'),
  // .matches(/^[^\x20-\x7e]*$/, '全角文字のみ'),
  author: Yup.string()
    .max(50, '50文字以下を入力してください。')
    .required('著者名を入力してください。'),
  // .matches(/^[^\x20-\x7e]*$/, '全角文字のみ'),
  publisher: Yup.string()
    .max(50, '50文字以下を入力してください。')
    .required('出版社名を入力してください。'),
  // .matches(/^[^\x20-\x7e]*$/, '全角文字のみ'),
  pubDate: Yup.date()
    .typeError('正しい日付を入力してください。')
    .required('出版日を入力してください。'),
})
