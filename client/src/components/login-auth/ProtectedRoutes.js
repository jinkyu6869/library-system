import { Outlet, Navigate } from 'react-router-dom'

const ProtectedRoutes = ({ isLogged }) => {
  return isLogged ? <Outlet /> : <Navigate to="/" />
}
export default ProtectedRoutes
