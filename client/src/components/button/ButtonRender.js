import { Link } from 'react-router-dom'
import { StyledFormButton } from '../../styles/Style'

function ButtonRender({ data, path }) {
  return (
    <div>
      <Link onClick={data.logout} to={data.to}>
        <StyledFormButton
          className={
            // メニューで現在画面のボタンの背景色が変わる
            data.to === path ? 'bg-primary bg-gradient text-white' : ''
          }
          style={{ fontSize: '15px', width: '120px', padding: '5px' }}
        >
          {data.title}
        </StyledFormButton>
      </Link>{' '}
      <br />
    </div>
  )
}

export default ButtonRender
