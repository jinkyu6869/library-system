import Tippy from '@tippyjs/react/headless'
import 'tippy.js/dist/tippy.css'
import { Wrapper } from './MenuItems'
import ButtonRender from './ButtonRender'

function MenuButton({ children, items = [], path }) {
  const renderItems = () => {
    return items.map((item, index) => (
      <ButtonRender key={index} data={item} path={path} />
    ))
  }
  return (
    <Tippy
      interactive
      delay={[0, 700]}
      offset={[-15, 8]}
      theme="light"
      placement="bottom-end"
      render={(attrs) => (
        <div className="content" tabIndex="-1" {...attrs}>
          <Wrapper>{renderItems()}</Wrapper>
        </div>
      )}
    >
      {children}
    </Tippy>
  )
}

export default MenuButton
