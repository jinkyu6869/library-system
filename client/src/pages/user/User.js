import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import axios from 'axios'

import {
  StyledFormButton,
  StyledUserButton,
  CopyrightText,
} from '../../styles/Style'
import { useStore } from '../../App'
import ManagerButton from '../../components/button/ManagerButton'

const User = () => {
  const navigate = useNavigate()
  const [authorityCODE, setAuthorityCODE] = useState()

  const loadAuthorityCODE = async () => {
    const response = await axios.get('http://localhost:3001/get-authority-code')
    setAuthorityCODE(response.data.data)
  }
  useEffect(() => {
    loadAuthorityCODE()
  }, [])

  const handleLogout = () => {
    var r = window.confirm('ログアウトしますか？')
    if (r) {
      useStore.setState({ loginID: '' })
      navigate('/')
    }
  }

  return (
    <div>
      {/**ページタイトル */}
      <h1
        // className="mx-auto flex flex col justify-center items-center"
        style={{
          fontSize: 80,
        }}
      >
        マイライブラリ
      </h1>

      {/**ログアウトのボタンをセッティング
       * クリックしたらログインページに遷移
       */}
      <div
        style={{
          position: 'absolute',
          display: 'flex',
          top: '0.5rem',
          right: '2rem',
        }}
      >
        {authorityCODE === 0 && (
          <StyledFormButton onClick={handleLogout}>ログアウト</StyledFormButton>
        )}
      </div>
      {/**管理者ページへ遷移のボタンをセッティング*/}
      {authorityCODE === 1 && <ManagerButton path="/user" />}

      {/** ページのコンテント */}
      <div className="min-h-screen">
        <div>
          <StyledUserButton onClick={() => navigate('/search-books')}>
            図書検索・予約
          </StyledUserButton>
          <br />
          <Link to="/user-reserve">
            <StyledUserButton>予約状況</StyledUserButton>
          </Link>
          <br />
          <Link to="/user-rental">
            <StyledUserButton>貸出状況</StyledUserButton>
          </Link>
          <br />
          <StyledUserButton
            onClick={() => navigate('/edit-personal')}
            style={{
              width: '35%',
              float: 'left',
            }}
          >
            個人情報修正
          </StyledUserButton>
          <Link to="/change-password">
            <StyledUserButton
              style={{
                width: '40%',
                float: 'right',
                marginRight: 0,
              }}
            >
              パスワード変更
            </StyledUserButton>
          </Link>
        </div>
      </div>
      <div
        style={{
          bottom: 0,
          clear: 'right',
        }}
      >
        <CopyrightText>All rights reserved &copy;2022</CopyrightText>
      </div>
    </div>
  )
}

export default User
