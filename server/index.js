import express from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
import nodeMailer from 'nodemailer'
import db from './db.js'
const app = express()
app.use(express.urlencoded({ extended: true }))
app.use(cors())
app.use(express.json())
dotenv.config()

let checkID //データベースにIDがあるかどうか確認
let loginID
let authorityCODE

//login
app.post('/login', (req, res) => {
  const { id, password } = req.body
  loginID = id

  const sqlSel = `SELECT id, password, authority_CODE FROM user WHERE id=${id} and password="${password}"`
  console.log(sqlSel) //Check SQL query
  db.query(sqlSel, (err, result) => {
    if (result.length > 0) {
      authorityCODE = result.map((r) => r.authority_CODE)[0]
      console.log(result)
      res.send(result)
    } else {
      console.log({ message: 'wrong!' })
      res.send({ message: 'wrong!' })
    }
  })
})

//info check
app.post('/check-user', (req, res) => {
  const { id, email } = req.body
  console.log(id, email) //Check value of id, password
  checkID = id

  const sqlSel = `SELECT id, email FROM user WHERE id=${id} and email="${email}"`
  console.log(sqlSel) //Check SQL query
  db.query(sqlSel, (err, result) => {
    if (result.length > 0) {
      console.log(result)
      res.send(result)
    } else {
      console.log({ message: 'wrong!' })
      res.send({ message: 'wrong!' })
    }
  })
})

//passcheck & change
app.post('/change-password', (req, res) => {
  const password = req.body.password
  console.log('password:', password, '/ checkID:', checkID)
  const checkPass = `SELECT id FROM user WHERE password="${password}"`

  let checkPassID
  if (loginID !== 0) {
    checkPassID = loginID
  } else {
    checkPassID = checkID
  }

  let inputID
  db.query(checkPass, (err1, result1) => {
    console.log('69 ' + checkPassID)
    console.log('get: ' + result1.map((r) => r.id)[0])
    console.log(result1.length) //Check result1's value
    inputID = result1.map((r) => r.id)[0]
    console.log(checkPassID == checkID) //check conditional true or false
    let turn = inputID == checkPassID

    if (!turn) {
      const sqlChange = `UPDATE user SET password="${password}" WHERE id=${checkPassID}`
      db.query(sqlChange, (err, result) => {
        console.log(result)
        res.send({ message: 'change' })
      })
    } else {
      res.send({ message: 'error' })
    }
  })
})

//Send mail
app.post('/send-email', (req, res) => {
  const link = 'http://localhost:3000/info'
  let ID

  const { name, email } = req.body

  const sqlID = `SELECT id FROM user WHERE email="${email}"`
  db.query(sqlID, (err, result) => {
    ID = result.map((r) => r.id)[0]
  })

  nodeMailer.createTestAccount((err, account) => {
    const htmlEmail = `
            <h3>Hi, ${name} </h3>
            <p> This is your ID Login: ${ID} </p>
            <p>Please click below link to reset your account</p>
            <a href="${link}">Click</a>

        `
    let mailerConfig = {
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS,
      },
    }
    let transporter = nodeMailer.createTransport(mailerConfig)

    let mailOptions = {
      from: 'library@testemail.co',
      to: email,
      subject: 'RESET YOUR ACCOUNT',
      html: htmlEmail,
    }

    if (!ID) {
      res.send({ message: 'Wrong email!' })
    } else {
      transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
          res
            .status(500)
            .send({ status: 'FAIL', msg: 'Internal error: email not sent' })
        } else {
          res.status(200).json({ status: 'OK', msg: 'Email sent' })
          console.log('Message sent')
        }
      })
    }
  })
})

//getloginID
app.get('/get-login-id', (req, res) => {
  res.send({ data: loginID })
})
//get authCode
app.get('/get-authority-code', (req, res) => {
  res.send({ data: authorityCODE })
})

//manager page
app.get('/manager/get-users/:showFlag', (req, res) => {
  const { showFlag } = req.params
  const sqlSel = `SELECT id, name, name_kana, birthday, email, phone, gender, reg_ID, reg_date FROM user WHERE show_flag=${showFlag}`
  db.query(sqlSel, (err, result) => res.send(result))
})

//delete
app.delete('/manager/remove-users/:id', (req, res) => {
  const { id } = req.params
  console.log(id)
  const sqlRemove = `DELETE FROM user WHERE id in(${id}) `
  db.query(sqlRemove, (error, result) => {
    if (error) {
      console.log(error)
    }
  })
})

//hide
app.delete('/manager/toggle-hide-users/:id/:showFlag', (req, res) => {
  const { id, showFlag } = req.params
  const invertedShowFlag = showFlag == 0 ? 1 : 0
  const sqlToggleHide = `UPDATE user SET show_flag=${invertedShowFlag} WHERE id in (${id})`
  db.query(sqlToggleHide, (error, result) => res.send({ message: 'updated' }))
})

//register
app.post('/manager/register-user', (req, res) => {
  const {
    id,
    password,
    name,
    nameKana,
    birthday,
    gender,
    email,
    phone,
    postCode,
    address,
    authorityCODE,
    regID,
  } = req.body

  db.query(
    'INSERT INTO user (id, name, name_kana, gender, birthday, email, phone, authority_CODE, password, postcode, address, reg_ID, reg_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,now())',
    [
      id,
      name,
      nameKana,
      gender,
      birthday,
      email,
      phone,
      authorityCODE,
      password,
      postCode,
      address,
      regID,
    ],
    (err, result) => {
      if (err) {
        console.log(err)
        if (err.sqlMessage.includes('PRIMARY')) {
          res.send({ message: 'dup id' })
        } else if (err.sqlMessage.includes('email_UNIQUE')) {
          res.send({ message: 'dup email' })
        } else if (err.sqlMessage.includes('phone_UNIQUE')) {
          res.send({ message: 'dup phone' })
        } else {
          res.send(err)
        }
      } else {
        res.send({ message: 'inserted' })
        console.log('会員情報登録成功')
      }
    }
  )
})

//get, update information for edit row
app.get('/get-user/:selectedID', (req, res) => {
  const { selectedID } = req.params
  db.query(
    `SELECT *, date_format(birthday,'%Y-%m-%d') AS bday FROM user WHERE id = ${selectedID}`,
    (err, result) => {
      if (err) {
        res.send(err)
        console.log(err)
      } else {
        res.send(result)
      }
    }
  )
})

app.put('/manager/edit-user', (req, res) => {
  const {
    id,
    name,
    nameKana,
    birthday,
    gender,
    email,
    phone,
    postCode,
    address,
    updID,
  } = req.body

  db.query(
    'UPDATE user SET name = ?, name_kana = ?, birthday = ?, gender = ?, email = ?, phone = ?, postCode = ?, address = ?, upd_ID = ?, upd_date = now() WHERE id = ?',
    [
      name,
      nameKana,
      birthday,
      gender,
      email,
      phone,
      postCode,
      address,
      updID,
      id,
    ],
    (err, result) => {
      if (err) {
        console.log(err)
        if (err.sqlMessage.includes('email_UNIQUE')) {
          res.send({ message: 'dup email' })
        } else if (err.sqlMessage.includes('phone_UNIQUE')) {
          res.send({ message: 'dup phone' })
        } else {
          res.send(err)
        }
      } else {
        console.log('会員情報修正成功')
        res.send({ message: 'updated' })
      }
    }
  )
})

app.get('/user/search-books/:category/:keyword', (req, res) => {
  const { category, keyword } = req.params
  const sqlSel = `SELECT bk.*, count(rv.book_ID) as reserve_count, DATE_FORMAT(bk.pub_date, '%Y/%m/%d') AS pday,
    (CASE
    WHEN(bk.rental_flag = 1) THEN '貸出中'
    WHEN(bk.rental_flag = 0 AND count(rv.book_ID) > 0) THEN '予約中'
    ELSE '在架'
    END) AS book_status
    FROM book AS bk
    LEFT OUTER JOIN reservation AS rv ON bk.book_ID = rv.book_ID
    WHERE bk.${category} LIKE '%${keyword}%'
    GROUP BY bk.book_ID;`
  db.query(sqlSel, (err, result) => {
    if (err) {
      console.log(err)
      res.send(err)
    } else {
      res.send(result)
    }
  })
})

app.post('/user/reserve-book/:bookID/:loginID', (req, res) => {
  const { bookID, loginID } = req.params
  const CHECK_RENTAL = `SELECT id FROM rental WHERE user_ID = ${loginID} AND book_ID = ${bookID}`
  const REGISTER_RESERVE = `INSERT INTO reservation (book_ID, user_ID, reserved_date)
  SELECT ?,?,now() FROM DUAL WHERE (SELECT count(*) FROM reservation WHERE user_ID = ?) < 3`
  db.query(CHECK_RENTAL, (err, result) => {
    if (result.length > 0) {
      res.send({ message: 'rental-ing' })
      return
    } else {
      db.query(REGISTER_RESERVE, [bookID, loginID, loginID], (err, result) => {
        if (err) {
          console.log(err)
          if (err.sqlMessage.includes('entry')) {
            res.send({ message: 'dup entry' })
          } else {
            res.send(err)
          }
        } else {
          if (result.affectedRows === 0) {
            res.send({ message: 'reserve limit' })
            console.log('reserve limit')
          } else {
            res.send({ message: 'inserted' })
            console.log('Values Inserted')
          }
        }
      })
    }
  })
})

app.put('/user/edit-personal', (req, res) => {
  const { id, email, phone, postCode, address } = req.body
  db.query(
    'UPDATE user SET email = ?, phone = ?, postCode = ?, address = ?, upd_ID = ?, upd_date = now() WHERE id = ?',
    [email, phone, postCode, address, id, id],
    (err, result) => {
      if (err) {
        console.log(err)
        console.log('Values Not Updated')
        if (err.sqlMessage.includes('email_UNIQUE')) {
          res.send({ message: 'dup email' })
        } else if (err.sqlMessage.includes('phone_UNIQUE')) {
          res.send({ message: 'dup phone' })
        } else {
          res.send(err)
        }
      } else {
        res.send({ message: 'updated' })
        console.log('個人情報修正完成')
      }
    }
  )
})

//Reserve
app.get('/user/reserve-detail', (req, res) => {
  const SELECT_RESERVE_DETAIL =
    'select b.book_ID, b.title, b.author, b.publisher, b.rental_flag, o.reserved_date, o.status FROM book b JOIN reservation o ON b.book_ID = o.book_ID WHERE user_ID=?'
  db.query(SELECT_RESERVE_DETAIL, [loginID], (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.send(result)
    }
  })
})

app.get('/reserve-index/:bookID', (req, res) => {
  const userID = loginID
  const { bookID } = req.params
  console.log(userID)
  const SELECT_INDEX_RESERVE =
    'set @row_num = 0; select indexes from (select (@row_num := @row_num + 1) as indexes, book_ID, user_ID from reservation where book_ID = ? order BY book_ID) a where user_ID = ?;'
  db.query(SELECT_INDEX_RESERVE, [bookID, userID], (err, result) => {
    if (err) return console.log(err)
    console.log(result)
    const index = result[1][0].indexes
    console.log(index)
    res.send({ data: index })
  })
})

app.delete('/user/cancel-reserve/:bookID', (req, res) => {
  const { bookID } = req.params
  const DELETE_RESERVE = 'DELETE FROM reservation WHERE book_ID=? AND user_ID=?'
  db.query(DELETE_RESERVE, [bookID, loginID], (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.send(result)
    }
  })
})

//Rental
app.get('/rental-detail', (req, res) => {
  const SELECT_RENTAL_DETAIL =
    'SELECT b.book_ID, b.title, b.author, b.publisher, r.rent_date, r.return_date, r.ex_period_flag FROM book b JOIN rental r ON b.book_ID = r.book_ID WHERE user_ID=?'

  db.query(SELECT_RENTAL_DETAIL, [loginID], (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.send(result)
    }
  })
})

app.get('/period/checkPeriodFlag/:bookID', (req, res) => {
  const { bookID } = req.params
  const CHECK_BOOK_PERIOD = 'SELECT ex_period_flag FROM rental WHERE book_ID=?'
  const CHECK_RESERVE_WAITING = 'SELECT * FROM reservation WHERE book_ID = ?'

  db.query(CHECK_BOOK_PERIOD, [bookID], (err, result) => {
    const flag = result.map((r) => r.ex_period_flag)[0]
    if (flag === 0) {
      db.query(CHECK_RESERVE_WAITING, [bookID], (err, result2) => {
        if (result2.length !== 0) return res.send({ message: 'no' })
        res.send({ message: 'ok' })
        console.log('Can update')
      })
    } else {
      res.send({ message: 'no' })
    }
  })
})

app.put('/period/:bookID', (req, res) => {
  const { bookID } = req.params
  const UPDATE_PERIOD =
    'update rental set return_date = (select date_add(return_date, INTERVAL 15 DAY)), ex_period_flag = 1 where ex_period_flag = 0 and book_ID=?'
  db.query(UPDATE_PERIOD, [bookID], (err, result) => {
    if (result) {
      console.log('Update period')
    } else {
      res.send(error)
      console.log(error)
    }
  })
})

app.get('/manager/get-books', (req, res) => {
  const sqlSel = `SELECT bk.*, count(rv.book_ID) as reserve_count, DATE_FORMAT(bk.pub_date, '%Y/%m/%d') AS pday,
  (CASE
  WHEN(bk.rental_flag = 1) THEN '貸出中'
  WHEN(bk.rental_flag = 0 AND count(rv.book_ID) > 0) THEN '予約中'
  ELSE '在架'
  END) AS book_status
  FROM book AS bk
  LEFT OUTER JOIN reservation AS rv ON bk.book_ID = rv.book_ID
  GROUP BY bk.book_ID`
  db.query(sqlSel, (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.send(result)
      // console.log(result)
    }
  })
})

app.post('/manager/register-book', (req, res) => {
  const { title, author, publisher, pubDate, regID } = req.body

  db.query(
    'INSERT INTO book (title,author,publisher,pub_Date, reg_ID, reg_date) VALUES (?,?,?,?,?,now())',
    [title, author, publisher, pubDate, regID],
    (err, result) => {
      if (err) {
        console.log(err)
        res.send(err)
      } else {
        res.send({ message: 'inserted' })
        console.log('図書情報登録成功')
      }
    }
  )
})

app.delete('/manager/remove-book/:bookID', (req, res) => {
  const { bookID } = req.params
  console.log(bookID)
  const sqlRemove = `DELETE FROM book WHERE book_ID = ${bookID}
  AND rental_flag != 1
  AND ${bookID} NOT IN (SELECT book_ID FROM reservation)
  `
  db.query(sqlRemove, (error, result) => {
    if (error) {
      console.log(error)
    } else if (!result.affectedRows) {
      // } else if (result.affectedRows === 0) {
      res.send({ message: 'in use' })
    } else {
      res.send({ message: 'deleted' })
      console.log('図書情報削除成功')
    }
  })
})

app.get('/get-book/:selectedID', (req, res) => {
  const { selectedID } = req.params
  db.query(
    `SELECT * FROM book WHERE book_ID = ${selectedID}`,
    (err, result) => {
      if (err) {
        console.log(err)
      } else {
        res.send(result)
      }
    }
  )
})

// app.put('/manager/edit-book', (req, res) => {
//   const { title, author, publisher, pubDate, updID, bookID } = req.body

//   db.query(
//     'UPDATE book SET title = ?, author = ?, publisher = ?, pub_date = ?, upd_ID = ?, upd_date = now() WHERE book_ID = ?',
//     [title, author, publisher, pubDate, updID, bookID],
//     (err, result) => {
//       if (err) {
//         console.log(err)
//         res.send(err)
//       } else {
//         console.log('Values Updated')
//         res.send({ message: 'updated' })
//       }
//     }
//   )
// })

//Reserve Manager
app.get('/manager/get-reservation', (req, res) => {
  const SELECT_ALL_ORDER =
    'SELECT r.user_ID, r.book_ID, b.title, b.author, b.publisher, b.rental_flag, r.reserved_date, r.status from reservation r join book b on r.book_ID = b.book_ID GROUP BY r.book_ID ORDER BY r.reserved_date'
  db.query(SELECT_ALL_ORDER, (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.send(result)
    }
  })
})

app.put('/reserve-status-change/:bookID/:userID', (req, res) => {
  const { bookID, userID } = req.params
  const CHECK_AVAIABLE_FLAG = 'SELECT rental_flag from book where book_ID=?'
  db.query(CHECK_AVAIABLE_FLAG, [bookID], (err, result) => {
    if (!result) return console.log(err)
    const avaiableFlag = result[0].rental_flag
    console.log(avaiableFlag === 0)
    if (avaiableFlag !== 0) return res.send({ message: 'rent-ing' })
    const CHANGE_STATUS_RESERVE =
      'UPDATE reservation set status = 1 WHERE book_ID=? and user_ID=?'
    db.query(CHANGE_STATUS_RESERVE, [bookID, userID], (err, result) => {
      if (err) {
        console.log(err)
      } else {
        res.send(result)
      }
    })
  })
})

app.delete('/manager/cancel-reserve/:bookID/:userID', (req, res) => {
  const { bookID, userID } = req.params
  const DELETE_RESERVE = 'DELETE FROM reservation WHERE book_ID=? AND user_ID=?'
  db.query(DELETE_RESERVE, [bookID, userID], (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.send(result)
    }
  })
})

app.get('/manager/rental-book-search/:bookID/:userID', (req, res) => {
  const { bookID, userID } = req.params
  const CHECK_RENTAL_ING = `SELECT book_ID FROM rental WHERE book_ID = ${bookID}`
  const CHECK_RESERVE_ING = `SELECT book_ID FROM reservation WHERE book_ID = ${bookID}`
  const CHECK_RESERVE_SAME = `SELECT book_ID FROM reservation WHERE book_ID = ${bookID} AND user_ID = ${userID} AND status = 1`
  const SELECT_BOOK_FOR_RENTAL = `SELECT book_ID, title, author, publisher
    FROM book WHERE book_ID = ${bookID}`
  db.query(CHECK_RENTAL_ING, (err, result) => {
    if (err) return
    if (result.length !== 0) {
      res.send({ message: 'rental-ing' })
      return
    } else {
      db.query(CHECK_RESERVE_ING, (err, result) => {
        if (err) return
        if (result.length !== 0) {
          db.query(CHECK_RESERVE_SAME, (err, result) => {
            if (err) return
            if (result.length === 0) {
              res.send({ message: 'reserve-ing' })
              return
            } else {
              db.query(SELECT_BOOK_FOR_RENTAL, (err, result) => {
                if (err) return
                console.log(result)
                res.send(result)
              })
            }
          })
        } else {
          db.query(SELECT_BOOK_FOR_RENTAL, (err, result) => {
            if (err) return
            console.log(result)
            res.send(result)
          })
        }
      })
    }
  })
})

//Manager rental
app.get('/manager/rental-detail', (req, res) => {
  const SELECT_ALL_RENTAL =
    'SELECT r.user_ID, r.book_ID, b.title, b.author, b.publisher, r.rent_date, r.return_date, r.ex_period_flag, r.reg_ID, r.reg_date from rental r join book b on r.book_ID = b.book_ID'
  db.query(SELECT_ALL_RENTAL, (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.send(result)
    }
  })
})

app.get('/manager/check-user/:userID', (req, res) => {
  const { userID } = req.params
  const CHECK_USER = `SELECT id FROM library_system.user WHERE id = ${userID}`
  db.query(CHECK_USER, (err, result) => {
    if (err) return
    if (result.length !== 0) {
      res.send(result)
    } else {
      res.send({ message: 'not-user' })
    }
  })
})

app.get('/manager/check-can-rental/:userID', (req, res) => {
  const { userID } = req.params
  const CHECK_COUNT_FIVE = `SELECT COUNT(user_ID) AS counts FROM rental WHERE user_ID = ${userID}`
  const CHECK_OVERDUE = `SELECT return_date FROM library_system.rental WHERE user_ID = ${userID} ORDER BY return_date ASC LIMIT 1`
  db.query(CHECK_COUNT_FIVE, (err, result) => {
    if (err) return
    if (result[0].counts >= 5) {
      res.send({ message: 'counts-over-five' })
    } else if (result[0].counts === 0) {
      res.send(result)
    } else {
      db.query(CHECK_OVERDUE, (err, result) => {
        if (err) return
        const returnDate = result[0].return_date
        const today = new Date()
        returnDate.setHours(0, 0, 0, 0)
        today.setHours(0, 0, 0, 0)
        if (returnDate < today) {
          res.send({ message: 'overdue' })
        } else {
          res.send(result)
        }
      })
    }
  })
})

app.put('/manager/register-rental', (req, res) => {
  const { userID, bookID, rentDate, returnDate } = req.body
  console.log(userID, bookID, rentDate, returnDate)
  // const CHECK_RENTAL_POSIBLE = 'SELECT * FROM rental WHERE book_ID=?'
  const INSERT_RENTAL =
    'INSERT INTO rental (user_ID, book_ID, rent_date, return_date, reg_ID, reg_date) VALUES (?,?,?,?,?,?); UPDATE book set rental_flag =1 WHERE book_ID=?;DELETE FROM reservation WHERE book_ID=? AND user_ID=?;'
  // db.query(CHECK_RENTAL_POSIBLE, [bookID], (err1, result1) => {
  //   if (result1.length !== 0) return res.send({ message: 'rent-ing' })
  db.query(
    INSERT_RENTAL,
    [
      userID,
      bookID,
      rentDate,
      returnDate,
      loginID,
      rentDate,
      bookID,
      bookID,
      userID,
    ],
    (err, result) => {
      if (err) {
        console.log(err)
      } else {
        console.log(result)
        res.send(result)
      }
    }
  )
})
// })

app.delete('/rental-delete/:bookID/:userID', (req, res) => {
  const { bookID, userID } = req.params
  console.log(bookID)
  const DELETE_RENTAL =
    'DELETE FROM rental WHERE book_ID=? AND user_ID=?; UPDATE book SET rental_flag =0 WHERE book_ID=?;'
  const CHECK_RESERVE = 'SELECT user_ID FROM reservation WHERE book_ID = ?'
  db.query(DELETE_RENTAL, [bookID, userID, bookID], (err, result) => {
    if (err) {
      console.log(err)
    } else {
      console.log(bookID)
      db.query(CHECK_RESERVE, [bookID], (err1, result1) => {
        if (result1.length !== 0) res.send({ message: 'reserv-ing' })
      })
    }
  })
})

app.get('/test', (req, res) => {
  const sqlSel = `SELECT DATE(return_date, '%Y/%m/%d') AS return_date FROM rental`
  db.query(sqlSel, (err, result) => {
    if (err) {
      return
    }
    res.send(result)
  })
})

app.listen(3001, () => {
  console.log('Listening from server 3001')
})
