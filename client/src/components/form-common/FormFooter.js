import Button from 'react-bootstrap/Button'

export default function FormFooter({ navigate, reset }) {
  return (
    <div>
      <div className="border-bottom mt-2" style={{ margin: '-16px' }}></div>
      <div className="float-start" style={{ marginTop: '30px' }}>
        {/* 戻るボタン。個人情報修正画面では見えないように。 */}
        {!navigate || (
          <Button
            variant="outline-secondary"
            onClick={() => {
              // 画面へ戻る
              navigate()
            }}
          >
            戻る
          </Button>
        )}
      </div>
      <div className="float-end" style={{ marginTop: '30px' }}>
        {/* リセットボタン */}
        <Button
          variant="danger"
          onClick={() => {
            reset()
          }}
        >
          リセット
        </Button>
        {/* 確認ボタン、submit */}
        <Button type="submit" className="ms-2" variant="primary">
          確認
        </Button>
      </div>
    </div>
  )
}
