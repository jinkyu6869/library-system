// ライブラリー
import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
// 自作コンポーネント
import UserFormList from '../../../components/form-user/UserFormList'
import FormFooter from '../../../components/form-common/FormFooter'
import UserConfirmModal from '../../../components/form-user/UserConfirmModal'
import { userSchema } from '../../../components/form-common/Validation'
import AuthCheckBox from '../../../components/form-user/AuthCheckBox'
import { useStore } from '../../../App'
import { formatDate } from '../../../components/form-common/DatePicker'

// 会員情報登録画面
export default function RegisterUser() {
  //取得したログインID変数
  const { loginID } = useStore()
  //入力情報変数
  const [userID, setUserID] = useState()
  const [password, setPassword] = useState('')
  const [name, setName] = useState('')
  const [nameKana, setNameKana] = useState('')
  const [birthday, setBirthday] = useState('')
  const [gender, setGender] = useState('m')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [postCode, setPostCode] = useState('')
  const [address, setAddress] = useState('')
  const [authorityCODE, setAuthorityCODE] = useState(false)
  // 画面遷移 hook
  const navigate = useNavigate()
  // react hook form
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(userSchema),
  })

  //入力した会員情報をDBに登録する関数
  const addUser = () => {
    axios
      .post('http://localhost:3001/manager/register-user', {
        id: userID,
        password: password,
        name: name,
        nameKana: nameKana,
        birthday: birthday,
        gender: gender,
        email: email,
        phone: phone,
        postCode: postCode,
        address: address,
        authorityCODE: authorityCODE,
        regID: loginID,
      })
      .then((response) => {
        // DBエラーメッセージ表示
        const r = response.data.message
        if (r === 'inserted') {
          alert('登録完了')
          // 管理者画面に遷移
          navigate('/manager-user')
        } else {
          if (r === 'dup id') {
            alert('IDが重複しています。')
          } else if (r === 'dup email') {
            alert('メールアドレスが重複しています。')
          } else if (r === 'dup phone') {
            alert('電話番号が重複しています。')
          } else {
            alert(
              '再度ログインしもできない場合は、担当者までお問い合わせください。'
            )
          }
          // モーダルを閉じる
          handleClose()
          // 確定ボタンを再活性化
          handleActive()
        }
      })
  }

  //モーダル制御
  const [show, setShow] = useState(false)
  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  //確定ボタン制御
  const [active, setActive] = useState(false)
  const handleInactive = () => setActive(true)
  const handleActive = () => setActive(false)

  // submit時の処理
  const onSubmit = (input) => {
    // ID: 10桁の数字をgetTimeメソッドを使って生成。Int型の範囲を考慮し、6630…を引くことで長く利用できるようにする。
    setUserID(
      parseInt(String(new Date().getTime() - 663000000000).slice(0, 10))
    )
    // 生年月日の連番をパスワードに設定
    setPassword(formatDate(input.birthday).replace(/-/g, ''))
    setName(input.name)
    setNameKana(input.nameKana)
    setBirthday(formatDate(input.birthday))
    setGender(input.gender)
    setEmail(input.email)
    setPhone(input.phone)
    setPostCode(input.postCode)
    setAddress(input.address)
    // checked(true): 1(管理者), unchecked(false): 0(一般会員)
    setAuthorityCODE(input.authorityCODE ? 1 : 0)
    // モーダルを開く
    handleShow()
  }

  return (
    <div className="card my-5 mx-auto" style={{ width: '25rem' }}>
      <div className="card-body">
        {/* フォームヘッダー */}
        <h4 className="mt-1">会員情報登録</h4>
        <div
          className="border-bottom mt-3 mb-2"
          style={{ margin: '-16px' }}
        ></div>
        <form
          // ブラウザーの基本バリデーション非活性化
          noValidate
          // handleSubmitでバリデーション ➔ モーダル表示
          onSubmit={handleSubmit((input) => {
            onSubmit(input)
          })}
        >
          {/* フォームリスト */}
          <UserFormList
            register={register}
            errors={errors}
            gender={gender}
            birthday
          />
          {/* 権限チェックボックス（登録画面のみ） */}
          <AuthCheckBox register={register} />
          {/* フォームフッター */}
          <FormFooter
            reset={reset}
            navigate={() => navigate('/manager-user')}
          />
        </form>
        {/* 確認モーダル */}
      </div>
      <UserConfirmModal
        modalName="会員情報登録確認"
        show={show}
        handleClose={handleClose}
        id={userID}
        name={name}
        nameKana={nameKana}
        birthday={birthday}
        gender={gender}
        email={email}
        phone={phone}
        postCode={postCode}
        address={address}
        authorityCODE={authorityCODE}
        // addUser: 登録関数
        func={addUser}
        active={active}
        handleInactive={handleInactive}
      />
    </div>
  )
}
